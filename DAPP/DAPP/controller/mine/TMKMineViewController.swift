//
//  TMKMineViewController.swift
//  TMKProcess
//
//  Created by yifutong on 2018/12/26.
//  Copyright © 2018 yifutong. All rights reserved.
//

import UIKit

class TMKMineViewController: TMKBaseViewController,UITableViewDataSource,UITableViewDelegate {

    var dataArray : NSArray!
    var tableView : UITableView!
    var moneyLabel : UILabel!
    var dataDict : NSDictionary!
    
    var avatorImgaeView : UIImageView!
    var avatorLabel : UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataDict = NSDictionary()
//        if get_userDefaults(key: "userInfo") is NSNull {
//            let alertController = UIAlertController(title: "入网提示", message: "您还没有申请商户入网，请开通后使用!", preferredStyle: .alert)
//            let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
//            let okAction = UIAlertAction(title: "开通", style: .default, handler: {
//                action in
//                self.navigationController?.pushViewController(TMKBindFirstViewController(), animated: true)
//            })
//            alertController.addAction(cancelAction)
//            alertController.addAction(okAction)
//            self.present(alertController, animated: true, completion: nil)
//        }else {
//            if  get_userDefaults(key: "userInfo") == nil ||  get_userDefaults(key: "userInfo") as! NSDictionary == [:]  {
//                let alertController = UIAlertController(title: "入网提示", message: "您还没有申请商户入网，请开通后使用!", preferredStyle: .alert)
//                let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
//                let okAction = UIAlertAction(title: "开通", style: .default, handler: {
//                    action in
//                    self.navigationController?.pushViewController(TMKBindFirstViewController(), animated: true)
//                })
//                alertController.addAction(cancelAction)
//                alertController.addAction(okAction)
//                self.present(alertController, animated: true, completion: nil)
//            }else {
//
//                self.dataDict =  (get_userDefaults(key: "userInfo") as! NSDictionary)
//            }
//        }
//        if self.dataDict.count > 0 {
            print("------------------------------")
            print(self.dataDict)
            let navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 64))
            self.view.addSubview(navBar)
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 180))
            headerView.backgroundColor = kRGBColorFromHex(rgbValue: mainColor)
            self.view.addSubview(headerView)

            self.avatorImgaeView = UIImageView() //frame: CGRect(x: 15, y: 35, width: 50, height: 50)
            headerView.addSubview(self.avatorImgaeView)

            self.avatorImgaeView.snp.makeConstraints { (make) in
                make.width.equalTo(90)
                make.height.equalTo(90)
                make.centerX.equalTo(headerView)
                make.top.equalTo(34)
            }
            
            self.avatorLabel = UILabel()
            self.avatorLabel.textAlignment = .center
            self.avatorLabel.textColor = UIColor.white
            self.avatorLabel.font = UIFont.systemFont(ofSize: 18)
            
            headerView.addSubview(self.avatorLabel)

            self.avatorLabel.snp.makeConstraints { (make) in
                make.top.equalTo(self.avatorImgaeView.snp.bottom).offset(10)
                make.left.equalTo(0)
                make.right.equalTo(0)
                make.height.equalTo(30)
            }
            
            self.dataArray = [["image":"mine_store","name":"个人信息","isNext":"0"],
                              ["image":"mine_trade","name":"卡包","isNext":"0"],
                              ["image":"mine_account","name":"我想反馈","isNext":"0"],
                              ["image":"mine_notice","name":"设置","isNext":"0"]]

            self.tableView = UITableView(frame: CGRect(x: 0, y: 180, width: screenWidth, height: screenHeight), style: .plain)
            self.tableView.backgroundColor = kRGBColorFromHex(rgbValue: bgMainColor)
            self.tableView.dataSource = self
            self.tableView.delegate = self
            self.tableView.separatorInset = .zero
            self.tableView.tableFooterView = UIView()
            self.view.addSubview(self.tableView)

            self.view.addSubview(nvac)
//        }
//
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
            case 0:
                let vc = TMKMerchantInfoViewController()
                self.navigationController?.pushViewController(vc, animated: true)
                break
            case 1:
                let vc = TMKBankListViewController()
                self.navigationController?.pushViewController(vc, animated: true)
                break
            case 2:
                let vc = TMKAccountViewController()
                self.navigationController?.pushViewController(vc, animated: true)
                break
            case 3:
                let vc = TMKSetViewController()
                self.navigationController?.pushViewController(vc, animated: true)
                break
            default:
                break
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "cell")
        cell.selectionStyle = .none
        let dict = self.dataArray[indexPath.row] as! NSDictionary
        cell.imageView?.image = UIImage(named: dict["image"] as! String)
        cell.textLabel?.text = dict["name"] as? String
        if dict["isNext"] as! String == "1" {
            //定义控件x:30 y:100 width:80 height:40
            let switcher = UISwitch(frame: CGRect(x: screenWidth - 60, y: 7, width: 30, height: 20))
            if get_userDefaultsStr(key: "isPlay") != nil && get_userDefaultsStr(key: "isPlay") != ""{
                switcher.isOn = true
            }else if get_userDefaultsStr(key: "isPlay") == ""{
                switcher.isOn = false
            }else {
                switcher.isOn = true
                save_userDefaultsStr(key: "isPlay", value: "1")
            }
        switcher.addTarget(self, action: #selector(switchBtnOn(swi:)), for: .touchUpInside)
        cell.contentView.addSubview(switcher)
            //设置开启状态显示的颜色
//            switcher.onTintColor = UIColor.red
            //设置关闭状态的颜色
//            switcher.tintColor = UIColor.green
            //滑块上小圆点的颜色
//            switcher.thumbTintColor = UIColor.yellow
            
        }else {
            cell.accessoryType = .disclosureIndicator//添加箭头
        }
        return cell
    }
    
    func getUserInfo() {
        self.showHUB()
        DAPPWebService.shareNetWorkBase.RequestParams(url: "android/getUserInfo", httpMethod: .HttpMethodPOST, params: [:], success: { (response) in
            print(response)
            self.stopHUB()
            let status = response["code"] as! String
            if status == "0000"{
                self.dataDict = (response["data"] as! NSDictionary)
//                let path = self.dataDict["headimg"] as! String
//                let url = URL(string: path)!
//                self.avatorImgaeView.kf.setImage(with: url)
                self.avatorImgaeView.image = UIImage(named: "mine_avator")
                self.avatorLabel.text = self.dataDict["nickname"] as? String
            }else {
                self.view.makeToast(response["msg"] as? String)
            }
        }) { (error) in
            print(error)
        }
    }
    //
    @objc func switchBtnOn(swi: UISwitch) {
        if swi.isOn {
            save_userDefaultsStr(key: "isPlay", value: "1")
        }else {
            save_userDefaultsStr(key: "isPlay", value: "")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.getUserInfo()
    }
    // 当时图已经消失时调用该方法
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
}
