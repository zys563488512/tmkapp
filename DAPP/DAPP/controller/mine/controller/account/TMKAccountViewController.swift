//
//  TMKAccountViewController.swift
//  TMKProcess
//
//  Created by yifutong on 2018/12/29.
//  Copyright © 2018 yifutong. All rights reserved.
//

import UIKit
import Charts
import ZWPlaceHolder

class TMKAccountViewController: TMKBaseViewController,UITableViewDataSource,UITableViewDelegate {

    var dataDict : NSDictionary!
    var tableView: UITableView!
    var dataArray: NSArray!
    
    var chooseLabel : UILabel!
    var inputText : UITextField!
    var textView : UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(nvac)
        self.title = "我想反馈"
        self.setupUI()
    }
    
    func setupUI() {
        self.tableView = UITableView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight - 144), style: .plain)
        self.tableView.backgroundColor = kRGBColorFromHex(rgbValue: 0xf1f1f1)
        self.tableView.separatorStyle = .none
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        self.view.addSubview(self.tableView)

        let tiButton = UIButton(frame: CGRect(x: 0, y: screenHeight - 114, width: screenWidth, height: 50))
        tiButton.backgroundColor = kRGBColorFromHex(rgbValue: mainColor)
        tiButton.setTitleColor(UIColor.white, for: .normal)
        tiButton.addTarget(self, action: #selector(getMoney), for: .touchUpInside)
        tiButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        tiButton.setTitle("提交", for: .normal)
        self.view.addSubview(tiButton)
    }
    @objc func getMoney() {
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 10
        case 1:
            return 44
        case 2:
            return 1
        case 3:
            return 44
        case 4:
            return 10
        case 5:
            return 150
        case 6:
            return 10
        case 7:
            return 44
        case 8:
            return 44
        default:
            break
        }
        return 44
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "cell")
        cell.selectionStyle = .none
        switch indexPath.row {
        case 0:
            cell.backgroundColor = kRGBColorFromHex(rgbValue: bgMainColor)
            break
        case 1:
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            cell.textLabel?.textColor = UIColor.black
            cell.accessoryType = .disclosureIndicator
            cell.textLabel?.text = "反馈类型"
            self.chooseLabel = UILabel(frame: CGRect(x: screenWidth - 220, y: 0, width: 190, height: 44))
            self.chooseLabel.textColor = UIColor.black
            self.chooseLabel.font = UIFont.systemFont(ofSize: 14)
            self.chooseLabel.text = "程序莫名异常"
            self.chooseLabel.textAlignment = .right
            cell.contentView.addSubview(self.chooseLabel)
            break
        case 2:
            cell.backgroundColor = kRGBColorFromHex(rgbValue: bgMainColor)
            break
        case 3:
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            cell.textLabel?.textColor = UIColor.black
            cell.textLabel?.text = "问题描述(至少5个字)"
            break
        case 4:
            cell.backgroundColor = kRGBColorFromHex(rgbValue: bgMainColor)
            break
        case 5:
//            let rect = CGRect(x: 5, y: 230, width: screenWidth - 10, height: 80)
            self.textView = UITextView(frame: CGRect(x: 15, y: 15, width: screenWidth - 30, height: 120))
            self.textView.layer.borderWidth = 1
            self.textView.font = UIFont.systemFont(ofSize: 14)
            self.textView.layer.borderColor = UIColor.lightGray.cgColor
            self.textView.zw_placeHolder = "请描述遇到的问题，以便我们及时为您解决"
//            self.textView.zw_limitCount = 30
//            textView.zw_placeHolderColor =
            cell.contentView.addSubview(self.textView)
            break
        case 6:
            cell.backgroundColor = kRGBColorFromHex(rgbValue: bgMainColor)
            break
        case 7:
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            cell.textLabel?.textColor = UIColor.black
            cell.textLabel?.text = "联系方式"
            self.inputText = UITextField(frame: CGRect(x: 100, y: 0, width: screenWidth - 130, height: 44))
            self.inputText.textColor = UIColor.black
            self.inputText.font = UIFont.systemFont(ofSize: 14)
            self.inputText.placeholder = "QQ/微信/手机号/邮箱"
            cell.contentView.addSubview(self.inputText)
            break
        default:
            break
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 1:
            let arr = ["程序莫名异常","程序响应时间长","交易异常","其他"]
            let sheetView = FWSheetView.sheet(title: "反馈类型", itemTitles: arr, itemBlock: { (index) in
                self.chooseLabel.text = arr[index] as String
            }, cancenlBlock: {
                print("点击了取消")
            })
            sheetView.show()
            break
        default:
            break
        }
    }
    func getAccountInfo() {
        let dict = ["content":self.textView.text!,"contact":self.inputText.text!,"FeedType":self.chooseLabel.text!]
        self.showHUB()
        DAPPWebService.shareNetWorkBase.RequestParams(url: "android/userFeed", httpMethod: .HttpMethodPOST, params: ["jsonPara":DictToJson.convertData(dict)], success: { (response) in
            print(response)
            self.stopHUB()
            let status = response["code"] as! String
            if status == "0000"{
                self.view.makeToast("反馈成功")
            }else {
                self.view.makeToast(response["msg"] as? String)
            }
        }) { (error) in
            print(error)
        }
    }
}
