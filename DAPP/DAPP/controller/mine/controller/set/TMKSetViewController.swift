//
//  TMKSetViewController.swift
//  TMKProcess
//
//  Created by yifutong on 2018/12/29.
//  Copyright © 2018 yifutong. All rights reserved.
//

import UIKit

class TMKSetViewController: TMKBaseViewController ,UITableViewDataSource,UITableViewDelegate  {
    
    var dataArray : NSArray!
    var tableView : UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "设置"
        
        self.dataArray = [["image":"mine_set_version","name":"版本信息","isNext":"0"],
                          ["image":"mine_set_exit","name":"退出","isNext":"0"]]
        
        self.tableView = UITableView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight), style: .grouped)
        self.tableView.backgroundColor = kRGBColorFromHex(rgbValue: bgMainColor)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.separatorInset = .zero
        self.tableView.tableHeaderView = UIView()
        self.tableView.tableFooterView = UIView()
        self.view.addSubview(self.tableView)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            break
        case 1:
            remove_userDefaults(key: "token")
            remove_userDefaults(key: "qrCode")
            remove_userDefaults(key: "userInfo")
            remove_userDefaults(key: "deviceToken")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kShowLoginPageNotification), object:  nil)
            break
        default:
            break
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "cell")
        cell.selectionStyle = .none
        let dict = self.dataArray[indexPath.row] as! NSDictionary
        cell.imageView?.image = UIImage(named: dict["image"] as! String)
        cell.textLabel?.text = dict["name"] as? String
        if indexPath.row == 0 {
            let textLabel = UILabel(frame: CGRect(x: screenWidth - 115, y: 0, width: 100, height: 44))
            textLabel.textAlignment = .right
            textLabel.font = UIFont.systemFont(ofSize: 13)
            textLabel.text = "\(appVersion)"
            textLabel.textColor = UIColor.gray
            cell.contentView.addSubview(textLabel)
        }
        
        return cell
    }
    
    //
    @objc func switchBtnOn(switch: UISwitch) {
        
    }
    
    @objc func getMoney() {
        
    }
}
