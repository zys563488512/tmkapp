//
//  TMKDepositViewController.swift
//  DAPP
//
//  Created by yifutong on 2019/1/4.
//  Copyright © 2019 zys. All rights reserved.
//

import UIKit

class TMKDepositViewController: TMKBaseViewController,UITableViewDataSource,UITableViewDelegate {

    var tableView : UITableView!
    var bankImageView : UIImageView!
    var bankNameLabel : UILabel!
    var depositMoneyTextField : UITextField!
    var keLabel:UILabel!
    var allMoney:Float!
    var bankInfo:NSDictionary!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "提现"
        
        let button1 = UIButton(frame:CGRect(x:0, y:0, width:18, height:18))
        button1.setTitle("提现记录", for: .normal)
        button1.setTitleColor(UIColor.white, for: .normal)
        button1.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button1.addTarget(self,action:#selector(depositList),for:.touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button1)
        self.getBankList()
        self.setupUI()
    }
    
    func setupUI() {
        self.tableView = UITableView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight - 114), style: .plain)
        self.tableView.backgroundColor = kRGBColorFromHex(rgbValue: bgMainColor)
        self.tableView.separatorStyle = .none
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.tableFooterView = UIView()
        self.view.addSubview(self.tableView)
        
        let footButton = UIButton(frame: CGRect(x: 0, y: screenHeight - 114, width: screenWidth, height: 50))
        footButton.setTitleColor(UIColor.white, for: .normal)
        footButton.backgroundColor = kRGBColorFromHex(rgbValue: mainColor)
        footButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        footButton.addTarget(self, action: #selector(nextButton), for: .touchUpInside)
        footButton.setTitle("立即提现", for: .normal)
        self.view.addSubview(footButton)
    }
    @objc func nextButton() {
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.bankInfo == nil {
            return 0
        }
        return 7
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 10
        case 1:
            return 44
        case 2:
            return 10
        case 3:
            return 30
        case 4:
            return 50
        case 5:
            return 1
        case 6:
            return 44
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "cell")
        cell.selectionStyle = .none
        cell.backgroundColor = kRGBColorFromHex(rgbValue: bgMainColor)
        switch indexPath.row {
        case 1:
            cell.backgroundColor = UIColor.white
            bankImageView = UIImageView(frame: CGRect(x: 15, y: 7, width: 30, height: 30))
            let url = URL(string: "http://picture.zhaohangzhifu.com/underline/bank_icon/\(bankInfo["bankAbbreviation"] as! String).png")
            bankImageView.kf.setImage(with: url)
            cell.contentView.addSubview(bankImageView)
            
            bankNameLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 200, height: 44))
            bankNameLabel.font = UIFont.systemFont(ofSize: 14)
            let cardNo = bankInfo["cardNo"] as! String
            let bank = bankInfo["openBank"] as! String
            let str1 = cardNo.suffix(4)
            bankNameLabel.text = bank + "(" + str1 + ")"
            cell.contentView.addSubview(bankNameLabel)
    
            cell.accessoryType = .disclosureIndicator//添加箭头
            break
        case 3:
            cell.backgroundColor = UIColor.white
            let tipsLabel = UILabel(frame: CGRect(x: 15, y: 0, width: 100, height: 30))
            tipsLabel.font = UIFont.systemFont(ofSize: 14)
            tipsLabel.textColor = UIColor.gray
            tipsLabel.text = "提现金额"
            cell.contentView.addSubview(tipsLabel)
            break
        case 4:
            cell.backgroundColor = UIColor.white
            let fuLabel = UILabel(frame: CGRect(x: 15, y: 20, width: 20, height: 20))
            fuLabel.font = UIFont.systemFont(ofSize: 20)
            fuLabel.text = "¥"
            cell.contentView.addSubview(fuLabel)
            self.depositMoneyTextField = UITextField(frame: CGRect(x: 40, y: 10, width: 300, height: 30))
            self.depositMoneyTextField.keyboardType = .decimalPad
            self.depositMoneyTextField.placeholder = "提现金额"
            self.depositMoneyTextField.font = UIFont.boldSystemFont(ofSize: 30)
            cell.contentView.addSubview(self.depositMoneyTextField)
            break
        case 6:
            cell.backgroundColor = UIColor.white
            keLabel = UILabel(frame: CGRect(x: 15, y: 7, width: 200, height: 30))
            keLabel.font = UIFont.systemFont(ofSize: 14)
            keLabel.textColor = UIColor.gray
            keLabel.text = "可用余额\(String(self.allMoney))元"
            cell.contentView.addSubview(keLabel)
            let allDepositButton = UIButton(frame: CGRect(x: screenWidth - 75, y: 0, width: 60, height: 44))
            allDepositButton.setTitleColor(kRGBColorFromHex(rgbValue: mainColor), for: .normal)
            allDepositButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            allDepositButton.setTitle("全部提现", for: .normal)
            allDepositButton.addTarget(self, action: #selector(self.allDeposit), for: .touchUpInside)
            cell.contentView.addSubview(allDepositButton)
            break
        default:
            break
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            let vc = TMKBankListViewController()
            vc.isNeed = "1"
            vc.myColsure = {(_ bankInfo: NSDictionary) -> Void in
                self.bankInfo = bankInfo
                self.tableView.reloadData()
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    //全部提现
    @objc func allDeposit() {
        self.depositMoneyTextField.text = String(self.allMoney)
    }
    //银行卡
    func getBankList() {
        self.showHUB()
        DAPPWebService.shareNetWorkBase.RequestParams(url: "mch/bankcard/select", httpMethod: .HttpMethodGET, params: [:], success: { (response) in
            print(response)
            self.stopHUB()
            let status = response["code"] as! String
            if status == "0000"{
                let arr  = (response["data"] as! NSArray)
                self.bankInfo = (arr[0] as! NSDictionary)
                self.tableView.reloadData()
            }else {
                self.view.makeToast(response["msg"] as? String)
            }
        }) { (error) in
            print(error)
        }
    }
    @objc func depositList() {
        
    }
}
