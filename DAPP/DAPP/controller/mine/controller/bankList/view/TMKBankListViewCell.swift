//
//  TMKBankListViewCell.swift
//  DAPP
//
//  Created by yifutong on 2019/1/4.
//  Copyright © 2019 zys. All rights reserved.
//

import UIKit

class TMKBankListViewCell: UITableViewCell {
    
    var centerView : UIView!
    var bankImageView : UIImageView!
    var bankNameLabel : UILabel!
    var bankNumberLabel : UILabel!
    var deleteButton : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        self.backgroundColor = kRGBColorFromHex(rgbValue: bgMainColor)
        
        self.centerView = UIView()
        self.centerView.backgroundColor = randomRGB()
        self.centerView.layer.masksToBounds = true
        self.centerView.layer.cornerRadius = 8
        self.contentView.addSubview(self.centerView)
        self.bankImageView = UIImageView()
        self.bankImageView.backgroundColor = UIColor.white
        self.bankImageView.layer.masksToBounds = true
        self.bankImageView.layer.cornerRadius = 40/2
        self.centerView.addSubview(self.bankImageView)
        
        self.bankNameLabel = UILabel()
        self.bankNameLabel.font = UIFont.systemFont(ofSize: 15)
        self.bankNameLabel.textColor = UIColor.white
        self.centerView.addSubview(self.bankNameLabel)
        
        self.bankNumberLabel = UILabel()
        self.bankNumberLabel.textColor = UIColor.white
        self.bankNumberLabel.font = UIFont.systemFont(ofSize: 17)
        self.centerView.addSubview(self.bankNumberLabel)
        
        self.deleteButton = UIButton()
        self.deleteButton.setTitleColor(UIColor.white, for: .normal)
        self.deleteButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        self.deleteButton.setTitle("解绑", for: .normal)
        self.centerView.addSubview(self.deleteButton)
        self.centerView.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.right.equalTo(-15)
            make.height.equalTo(90)
            make.top.equalTo(5)
        }
        self.bankImageView.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(15)
            make.width.equalTo(40)
            make.height.equalTo(40)
        }
        self.bankNameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.bankImageView.snp.right).offset(15)
            make.top.equalTo(self.bankImageView.snp.top)
            make.height.equalTo(self.bankImageView.snp.height)
            make.width.equalTo(200)
        }
        self.bankNumberLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.bankNameLabel.snp.left)
            make.width.equalTo(300)
            make.height.equalTo(20)
            make.top.equalTo(self.bankNameLabel.snp.bottom).offset(0)
        }
        self.deleteButton.snp.makeConstraints { (make) in
            make.right.equalTo(-15)
            make.top.equalTo(15)
            make.height.equalTo(self.bankImageView.snp.height)
            make.width.equalTo(50)
        }
    }
    
    func setBnakInfo(dataDict:NSDictionary) {
        self.bankNameLabel.text = dataDict["account_bank"] as? String
        self.bankNumberLabel.text = dataDict["account_no"] as? String
        let bankName = dataDict["account_bank"] as! String
        let str = bankName.components(separatedBy: "行")
        let translatedPinyinStr = self.getBankName(str: str[0])
        let url = URL(string: "https://cards.yifubank.com/xiaochengxu_image/small_bank_icon/\(translatedPinyinStr)h.png")
        self.deleteButton.tag = Int(dataDict["card_id"] as! String)!
        self.bankImageView.kf.setImage(with: url)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func translateChineseStringToPyinyin(chineseStr:String) -> String {
        var translatedPinyinStr:String = ""
        let zhcnStrToTranslate:CFMutableString = NSMutableString(string: chineseStr)
        var translatedOk:Bool = CFStringTransform(zhcnStrToTranslate, nil, kCFStringTransformMandarinLatin, false)
        if translatedOk {
            let translatedPinyinWithAccents = zhcnStrToTranslate
            translatedOk = CFStringTransform(translatedPinyinWithAccents, nil, kCFStringTransformStripCombiningMarks, false)
            if translatedOk {
                translatedPinyinStr = translatedPinyinWithAccents as String
            }
        }
        return translatedPinyinStr
    }
    
    func getBankName(str: String) -> String {
        let aa = self.translateChineseStringToPyinyin(chineseStr: str)
        let array = aa.components(separatedBy:" ")
        var nstr = ""
        for item in array {
            let index3 = str.index(str.startIndex, offsetBy: 0)
            let index4 = str.index(str.startIndex, offsetBy: 1)
            nstr = nstr + item[index3..<index4]
        }
        return nstr
    }
}
