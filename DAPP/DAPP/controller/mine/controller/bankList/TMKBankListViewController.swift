//
//  TMKBankListViewController.swift
//  TMKProcess
//
//  Created by yifutong on 2018/12/29.
//  Copyright © 2018 yifutong. All rights reserved.
//

import UIKit
typealias MyColsure = (_ bankInfo: NSDictionary) -> Void
class TMKBankListViewController: TMKBaseViewController,UITableViewDataSource,UITableViewDelegate {

    var tableView : UITableView!
    var dataArray : NSArray!
    var isNeed : String!
    var myColsure : MyColsure!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "我的卡包"
        self.setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getBankList()
    }
    func setupUI() {
        self.dataArray = NSArray()
        self.tableView = UITableView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight - 114), style: .plain)
        self.tableView.register(TMKBankListViewCell.classForCoder(), forCellReuseIdentifier: "cell")
        self.tableView.backgroundColor = kRGBColorFromHex(rgbValue: bgMainColor)
        self.tableView.separatorStyle = .none
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        self.view.addSubview(self.tableView)
        
        let bottomBtn = UIButton(frame: CGRect(x: 0, y: screenHeight - 114, width: screenWidth, height: 50))
        bottomBtn.backgroundColor = kRGBColorFromHex(rgbValue: mainColor)
        bottomBtn.addTarget(self, action: #selector(addBnakInfo), for: .touchUpInside)
        bottomBtn.setTitleColor(UIColor.white, for: .normal)
        bottomBtn.setTitle("添加信用卡", for: .normal)
        self.view.addSubview(bottomBtn)
        
        self.view.addSubview(nvac)
    }
    @objc func addBnakInfo() {
        let vc = TMKAddBankViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as! TMKBankListViewCell
        cell.selectionStyle = .none
        cell.setBnakInfo(dataDict: self.dataArray[indexPath.row] as! NSDictionary)
        cell.deleteButton.addTarget(self, action: #selector(deleteBank(btn:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isNeed == "" || isNeed == nil{
            
        }else {
            let data = self.dataArray[indexPath.row] as! NSDictionary
            if self.myColsure != nil {
                self.myColsure!(data)
            }
            self.navigationController?.popViewController(animated: true)
        }
     
    }
    @objc func deleteBank(btn:UIButton) {
        if self.dataArray.count == 1 {
            self.view.makeToast("至少保留一张结算卡")
            return
        }
        let alertController = UIAlertController(title: "提示", message: "您确认删除该结算卡吗？", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "确定", style: .default, handler: {
            action in
            self.showHUB()
            DAPPWebService.shareNetWorkBase.RequestParams(url: "mch/bankcard/untying", httpMethod: .HttpMethodDELETE, params: ["cardNo":String(btn.tag)], success: { (response) in
                print(response)
                self.stopHUB()
                let status = response["code"] as! String
                if status == "0000"{
                    self.getBankList()
                }else {
                    self.view.makeToast(response["msg"] as? String)
                }
            }) { (error) in
                print(error)
            }
        })
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    func getBankList() {
        self.showHUB()
        DAPPWebService.shareNetWorkBase.RequestParams(url: "android/getCard", httpMethod: .HttpMethodPOST, params: [:], success: { (response) in
            print(response)
            self.stopHUB()
            let status = response["code"] as! String
            if status == "0000"{
                self.dataArray = (response["data"] as! NSArray)
                self.tableView.reloadData()
            }else {
                self.view.makeToast(response["msg"] as? String)
            }
        }) { (error) in
            print(error)
        }
    }
}
