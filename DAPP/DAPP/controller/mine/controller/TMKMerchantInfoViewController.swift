//
//  TMKMerchantInfoViewController.swift
//  TMKProcess
//
//  Created by yifutong on 2018/12/29.
//  Copyright © 2018 yifutong. All rights reserved.
//

import UIKit

class TMKMerchantInfoViewController: TMKBaseViewController,UITableViewDataSource,UITableViewDelegate  {
    
    var dataArray : NSArray!
    var tableView : UITableView!
    var mchNo : String!
    var dataDict : NSDictionary!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "商户信息"
        self.getMchInfo()
        self.dataArray = [["image":"mine_store","name":"商户编号","isNext":"0"],
                          ["image":"mine_account","name":"联系人","isNext":"0"],
                          ["image":"mine_trade","name":"身份证号","isNext":"0"],
                          ["image":"mine_notice","name":"银行快捷费率","isNext":"0"],
                          ["image":"mine_customservice","name":"结算银行","isNext":"0"],
                          ["image":"mine_customservice","name":"结算卡号","isNext":"0"]]
        
        self.tableView = UITableView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight - 64), style: .plain)
        self.tableView.backgroundColor = kRGBColorFromHex(rgbValue: bgMainColor)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.separatorInset = .zero
        self.tableView.tableFooterView = UIView()
        self.view.addSubview(self.tableView)
        self.view.addSubview(nvac)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.dataDict != nil{
            return self.dataArray.count
        }else {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            break
        case 1:
            break
        case 2:
            break
        case 3:
            break
        case 4:
            break
        default:
            break
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "cell")
        cell.selectionStyle = .none
        let dict = self.dataArray[indexPath.row] as! NSDictionary
        cell.textLabel?.text = dict["name"] as? String
        let reightLabel = UILabel(frame: CGRect(x: 105, y: 0, width: screenWidth - 120, height: 44))
        reightLabel.textAlignment = .right
        reightLabel.font = UIFont.systemFont(ofSize: 13)
        reightLabel.textColor = UIColor.gray
        cell.contentView.addSubview(reightLabel)
        
        switch indexPath.row {
        case 0:
            reightLabel.text = self.dataDict["mch_no"] as? String
            break
        case 1:
            reightLabel.text = self.dataDict["name"] as? String
            break
        case 2:
            reightLabel.text = self.dataDict["cert_no"] as? String
            break
        case 3:
            reightLabel.text = self.dataDict["union_rate"] as? String
            break
        case 4:
            reightLabel.text = self.dataDict["account_bank"] as? String
            break
        case 5:
            reightLabel.text = self.dataDict["account_no"] as? String
            break
        default:
            break
        }
        return cell
    }
    func getMchInfo() {
        self.showHUB()
        DAPPWebService.shareNetWorkBase.RequestParams(url: "android/getUserInfo", httpMethod: .HttpMethodPOST, params: [:], success: { (response) in
            print(response)
            self.stopHUB()
            let status = response["code"] as! String
            if status == "0000"{
                self.dataDict = (response["data"] as! NSDictionary)
                self.tableView.reloadData()
            }else {
                self.view.makeToast(response["msg"] as? String)
            }
        }) { (error) in
            print(error)
        }
    }
 }
