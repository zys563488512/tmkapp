//
//  TMKLoginViewController.swift
//  TMKProcess
//
//  Created by yifutong on 2018/12/27.
//  Copyright © 2018 yifutong. All rights reserved.
//

import UIKit
import Toast_Swift
class TMKLoginViewController: TMKBaseViewController {

   var mobileTextFiled: UITextField!
   var codeTextFiled: UITextField!
   var getCodeBtn : UIButton!
   var loginButton: UIButton!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "登录"
        self.view.addSubview(nvac)
        self.setupUI()
    }
    func setupUI() {
        
        self.mobileTextFiled = UITextField()
        self.mobileTextFiled.font = UIFont.systemFont(ofSize: 14)
        self.mobileTextFiled.placeholder = "请输入手机号"
        self.mobileTextFiled.keyboardType = .numberPad
        self.mobileTextFiled.clearButtonMode = .whileEditing
        self.mobileTextFiled.textColor = UIColor.black
        self.view.addSubview(self.mobileTextFiled)
        
        let moLine = UIView()
        moLine.backgroundColor = UIColor.gray
        self.view.addSubview(moLine)
        
        self.codeTextFiled = UITextField()
        self.codeTextFiled.font = UIFont.systemFont(ofSize: 14)
        self.codeTextFiled.placeholder = "短信验证码"
        self.codeTextFiled.textColor = UIColor.black
        self.codeTextFiled.keyboardType = .numberPad
        self.codeTextFiled.clearButtonMode = .whileEditing
        self.view.addSubview(self.codeTextFiled)
        
        let coLine = UIView()
        coLine.backgroundColor = UIColor.gray
        self.view.addSubview(coLine)
        
        self.getCodeBtn = UIButton()
        self.getCodeBtn.backgroundColor = kRGBColorFromHex(rgbValue: mainColor)
        self.getCodeBtn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        self.getCodeBtn.setTitleColor(UIColor.white, for: .normal)
        self.getCodeBtn.setTitle("获取验证码", for: .normal)
        self.getCodeBtn.addTarget(self, action: #selector(getCodeInfo), for: .touchUpInside)
        self.view.addSubview(self.getCodeBtn)
        
        self.loginButton = UIButton()
        self.loginButton.backgroundColor = kRGBColorFromHex(rgbValue: mainColor)
        self.loginButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        self.loginButton.setTitleColor(UIColor.white, for: .normal)
        self.loginButton.addTarget(self, action: #selector(login(_:)), for: .touchUpInside)
        self.loginButton.setTitle("登录", for: .normal)
        self.view.addSubview(self.loginButton)
        
        self.mobileTextFiled.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(50)
            make.width.equalTo(screenWidth - 30)
            make.height.equalTo(30)
        }
        
        moLine.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(self.mobileTextFiled.snp.bottom).offset(0)
            make.width.equalTo(self.mobileTextFiled.snp.width)
            make.height.equalTo(1)
        }
        
        self.codeTextFiled.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(moLine.snp.bottom).offset(15)
            make.width.equalTo(screenWidth - 130)
            make.height.equalTo(30)
        }
        
        coLine.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(self.codeTextFiled.snp.bottom).offset(0)
            make.width.equalTo(self.codeTextFiled.snp.width)
            make.height.equalTo(1)
        }
        
        self.getCodeBtn.snp.makeConstraints { (make) in
            make.left.equalTo(screenWidth - 100)
            make.top.equalTo(moLine.snp.bottom).offset(15)
            make.width.equalTo(85)
            make.height.equalTo(30)
        }
        
        self.loginButton.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(coLine.snp.bottom).offset(20)
            make.width.equalTo(screenWidth - 30)
            make.height.equalTo(40)
        }
        
        let bottomView = UIView()
        self.view.addSubview(bottomView)
        
        let leftLine = UIView()
        leftLine.backgroundColor = UIColor.gray
        self.view.addSubview(leftLine)
        
        let tipsLabel = UILabel()
        tipsLabel.font = UIFont.systemFont(ofSize: 13)
        tipsLabel.textColor = UIColor.gray
        tipsLabel.text = "第三方登录"
        tipsLabel.textAlignment = .center
        self.view.addSubview(tipsLabel)
        
        let rightLine = UIView()
        rightLine.backgroundColor = UIColor.gray
        self.view.addSubview(rightLine)
        
        let wxBtn = UIButton()
        self.view.addSubview(wxBtn)
        
        bottomView.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.bottom.equalTo(-150)
            make.width.equalTo(screenWidth/2 - 30)
            make.height.equalTo(100)
        }
        leftLine.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.width.equalTo(screenWidth)
            make.height.equalTo(1)
        }
    }
   
    @objc func login(_ sender: Any) {
        if mobileTextFiled.text == "" {
            self.view.makeToast("手机号码不能为空")
            return
        }
        if codeTextFiled.text == "" {
            self.view.makeToast("验证码不能为空")
            return
        }
        print("password ==== \(self.codeTextFiled.text!)")
        print("password ==== \(self.codeTextFiled.text!.md5HashToLower16Bit)")
        var deviceToken = get_userDefaultsStr(key: "deviceToken")
        if deviceToken == nil {
            deviceToken = ""
        }else {
            
        }
        let dict = ["mobile":self.mobileTextFiled.text!,"code":self.codeTextFiled.text!]
        //登录
        self.showHUB()
        DAPPWebService.shareNetWorkBase.RequestParams(url: "android/appLogin", httpMethod: .HttpMethodPOST, params: ["type":"1","jsonPara":DictToJson.convertData(dict)], success: { (response) in
            print(response)
            self.stopHUB()
            let status = response["code"] as! String
            if status == "0000"{
                
                let data = response["data"] as! NSDictionary
                let token = response["token"] as! String
                
                save_userDefaultsStr(key: "token", value: token)
                save_userDefaults(key: "userInfo", value: data)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kShowHomePageNotification), object: nil)
            }else {
                self.view.makeToast(response["msg"] as? String)
            }
        }) { (error) in
            print(error)
        }
    }
    @objc func getCodeInfo() {
        if mobileTextFiled.text == "" {
            self.view.makeToast("手机号码不能为空")
            return
        }
        let dict = ["mobile":self.mobileTextFiled.text!]
        //获取验证码
        self.showHUB()
        DAPPWebService.shareNetWorkBase.RequestParams(url: "android/mobileMsg", httpMethod: .HttpMethodPOST, params: ["jsonPara":DictToJson.convertData(dict)], success: { (response) in
            print(response)
            self.stopHUB()
            let status = response["code"] as! String
            if status == "0000"{
                self.getDownTime()
            }else {
                self.view.makeToast(response["msg"] as? String)
            }
        }) { (error) in
            print(error)
        }
    }
    func getDownTime() {
        var time = 60
        let codeTimer = DispatchSource.makeTimerSource(flags: .init(rawValue: 0), queue: DispatchQueue.global())
        codeTimer.schedule(deadline: .now(), repeating: .milliseconds(1000))  //此处方法与Swift 3.0 不同
        codeTimer.setEventHandler {
            
            time = time - 1
            
            DispatchQueue.main.async {
                self.getCodeBtn.isEnabled = false
                self.getCodeBtn.backgroundColor = UIColor.gray
            }
            
            if time < 0 {
                codeTimer.cancel()
                DispatchQueue.main.async {
                    self.getCodeBtn.isEnabled = true
                    self.getCodeBtn.backgroundColor = kRGBColorFromHex(rgbValue: mainColor)
                    self.getCodeBtn.setTitle("重新发送", for: .normal)
                }
                return
            }
            
            DispatchQueue.main.async {
                self.getCodeBtn.setTitle("\(time)", for: .normal)
            }
            
        }
        
        if #available(iOS 10.0, *) {
            codeTimer.activate()
        } else {
            // Fallback on earlier versions
        }
    }
}
