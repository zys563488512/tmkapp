//
//  TMKHomeViewController.swift
//  TMKProcess
//
//  Created by yifutong on 2018/12/28.
//  Copyright © 2018 yifutong. All rights reserved.
//

import UIKit
class TMKHomeViewController: TMKBaseViewController,CHCycleScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,HQPickerViewDelegate {
    
    var bankArray : NSArray!
    var qdArray : NSMutableArray!
    
    var cycleScrollView: CHCycleScrollView!
    var tableView : UITableView!
    var reightLabel : UILabel!
    var moneyText : UITextField!
    
    var currIndex: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "兆行宝"
        self.currIndex = 0
        self.bankArray = NSArray()
        self.qdArray = NSMutableArray()
        self.getBankList()
    }
    
    func setupUI() {
        self.cycleScrollView = CHCycleScrollView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenWidth/3 * 2), imageGroups: (self.bankArray as! [Any]))
        self.cycleScrollView.backgroundColor = kRGBColorFromHex(rgbValue: bgMainColor)
        cycleScrollView?.delegate = self
        
        self.tableView = UITableView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight), style: .plain)
        self.tableView.backgroundColor = UIColor.white
        self.tableView.separatorStyle = .none
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.tableFooterView = UIView()
        self.tableView.tableHeaderView = self.cycleScrollView
        self.view.addSubview(self.tableView)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 44
        case 1:
            return 44
        default:
            return 30
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "cell")
        cell.textLabel?.text = "cell ---- \(indexPath.row)"
        cell.selectionStyle = .none
        switch indexPath.row {
        case 0:
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            cell.textLabel?.text = "渠道"
            cell.accessoryType = .disclosureIndicator
            
            self.reightLabel = UILabel(frame: CGRect(x: 100, y: 0, width: screenWidth - 130, height: 44))
            self.reightLabel.font = UIFont.systemFont(ofSize: 14)
            self.reightLabel.textAlignment = .right
            self.reightLabel.text = "请选择"
            cell.contentView.addSubview(self.reightLabel)
            
            let lineLabel = UILabel(frame: CGRect(x: 0, y: 43, width: screenWidth, height: 1))
            lineLabel.backgroundColor = kRGBColorFromHex(rgbValue: bgMainColor)
            cell.contentView.addSubview(lineLabel)
            break
        case 1:
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            cell.textLabel?.text = "提现金额"
            
            self.moneyText = UITextField(frame: CGRect(x: 100, y: 0, width: screenWidth - 115, height: 44))
            self.moneyText.font = UIFont.systemFont(ofSize: 14)
            self.moneyText.keyboardType = .numberPad
            self.moneyText.placeholder = "请输入提现金额"
            cell.contentView.addSubview(self.moneyText)
            
            let lineLabel = UILabel(frame: CGRect(x: 0, y: 43, width: screenWidth, height: 1))
            lineLabel.backgroundColor = kRGBColorFromHex(rgbValue: bgMainColor)
            cell.contentView.addSubview(lineLabel)
            break
        case 2:
            cell.textLabel?.font = UIFont.systemFont(ofSize: 13)
            cell.textLabel?.textColor = kRGBColorFromHex(rgbValue: 0xcecece)
            cell.textLabel?.text = "请输入提现金额，单笔限额"
            break
        case 3:
            cell.textLabel?.font = UIFont.systemFont(ofSize: 13)
            cell.textLabel?.textColor = kRGBColorFromHex(rgbValue: 0xcecece)
            cell.textLabel?.text = "交易单日限额 "
            break
        case 4:
            cell.textLabel?.font = UIFont.systemFont(ofSize: 13)
            cell.textLabel?.textColor = kRGBColorFromHex(rgbValue: 0xcecece)
            cell.textLabel?.text = "交易时间限制 "
            break
        default:
            break
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let arr = NSMutableArray()
            for i in 0..<self.qdArray.count {
                let dict = (self.qdArray[i] as! NSDictionary)
                arr.add(dict["name"] as! String)
            }
            
            let picker = HQPickerView(frame: self.view.bounds)
            picker.delegate = self
            picker.customArr = (arr as! [Any])
            self.view.addSubview(picker)
            break
        default:
            break
        }
    }
    func cycleScrollView(_ cycleScrollView: CHCycleScrollView!, didSelectItemAt index: Int) {
        print("index = \(index)")
        self.currIndex = index
    }
    
    //MARK: 获取银行卡列表
    func getBankList() {
        self.showHUB()
        DAPPWebService.shareNetWorkBase.RequestParams(url: "android/getCard", httpMethod: .HttpMethodPOST, params: ["jsonPara":"{}"], success: { (response) in
            print("---------------")
            print(response)
            self.stopHUB()
            let status = response["code"] as! String
            if status == "0000"{
                self.bankArray = (response["data"] as! NSArray)
                self.setupUI()
                self.getQDList()
            }else {
                self.view.makeToast(response["msg"] as? String)
            }
        }) { (error) in
            print(error)
        }
    }
    
    //MARK: 获取渠道
    func getQDList() {
        self.showHUB()
        let bankDict = self.bankArray[self.currIndex] as! NSDictionary
        let dict = ["account_bank":bankDict["account_bank"]!,"card_id":bankDict["card_id"]!] as [String : Any]
        DAPPWebService.shareNetWorkBase.RequestParams(url: "android/getTunnelMsg", httpMethod: .HttpMethodPOST, params: ["jsonPara":DictToJson.convertData(dict)], success: { (response) in
            print("------android/getTunnelMsg--------")
            print(response)
            self.stopHUB()
            let status = response["code"] as! String
            if status == "0000"{
                self.getQDListInfo(arr: (response["data"] as! NSArray))
            }else {
                self.view.makeToast(response["msg"] as? String)
            }
        }) { (error) in
            print(error)
        }
    }
    func getQDListInfo(arr:NSArray) {
        self.showHUB()
        let bankDict = self.bankArray[self.currIndex] as! NSDictionary
        let dict = ["account_bank":bankDict["account_bank"]!,"card_id":bankDict["card_id"]!] as [String : Any]
        DAPPWebService.shareNetWorkBase.RequestParams(url: "mch/getTradeType", httpMethod: .HttpMethodPOST, params: ["jsonPara":DictToJson.convertData(dict)], success: { (response) in
            print("------android/getTunnelMsg--------")
            print(response)
            self.stopHUB()
            let status = response["code"] as! String
            if status == "0000"{
                let qdArr = (response["data"] as! NSArray)
                for i in 0..<arr.count {
                    let dic = arr[i] as! NSDictionary
                    for j in 0..<qdArr.count {
                        let qdDic = qdArr[j] as! NSDictionary
                        if (dic["tunnel_id"] as! String) == (qdDic["tunnel_id"] as! String) {
                            self.qdArray.add(qdDic)
                        }
                    }
                }
                print("-----渠道列表\(self)")
                self.tableView.reloadData()
            }else {
                self.view.makeToast(response["msg"] as? String)
            }
        }) { (error) in
            print(error)
        }
    }
    func pickerView(_ pickerView: UIPickerView!, didSelectText text: String!) {
//        self.bankNameLabel.text = text
    }
}
