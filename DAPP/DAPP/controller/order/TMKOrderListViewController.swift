//
//  TMKOrderListViewController.swift
//  DAPP
//
//  Created by yifutong on 2019/1/23.
//  Copyright © 2019 zys. All rights reserved.
//

import UIKit
import MJRefresh

class TMKOrderListViewController: TMKBaseViewController,UITableViewDataSource,UITableViewDelegate {
    
    var headerView : UIView!
    var timeTitleLabel : UILabel!
    var timeTipsLabel : UILabel!
    @objc var chooseTimeBtn : UIButton!
    
    var tableView : UITableView!
    var page: Int!
    var dataArray : NSMutableArray!
    var dateStr : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "账单"
        self.view.backgroundColor = kRGBColorFromHex(rgbValue: bgMainColor)
        self.dataArray = NSMutableArray()
        self.setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidLoad()
        self.tableView.mj_header.beginRefreshing()
    }
    func setupUI() {
        self.headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 900))
        self.view.addSubview(self.headerView)
        self.timeTitleLabel = UILabel(frame: CGRect(x: 15, y: 5, width: 120, height: 20))
        let date = Date()
        let str = date.getDateYM()
        dateStr = str
        print(str)
        self.timeTitleLabel.text = str
        self.headerView.addSubview(self.timeTitleLabel)
        self.timeTipsLabel = UILabel(frame: CGRect(x: 15, y: 30, width: 230, height: 20))
        self.timeTipsLabel.font = UIFont.systemFont(ofSize: 12)
        self.timeTipsLabel.textColor = kRGBColorFromHex(rgbValue: 0xc0c0c0)
        self.headerView.addSubview(self.timeTipsLabel)
        self.chooseTimeBtn = UIButton(frame: CGRect(x: screenWidth - 50, y: 10, width: 30, height: 30))
        self.chooseTimeBtn.addTarget(self, action: #selector(chooseTime), for: .touchUpInside)
        self.chooseTimeBtn.setImage(UIImage(named: "order_date"), for: .normal)
        self.headerView.addSubview(self.chooseTimeBtn)
 
        self.tableView = UITableView(frame: CGRect(x: 0, y: 50, width: screenWidth, height: screenHeight - 114), style: .plain)
        self.tableView.register(TMKTradingListCell.classForCoder(), forCellReuseIdentifier: "cell")
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.backgroundColor = kRGBColorFromHex(rgbValue: bgMainColor)
        self.tableView.mj_header = MJRefreshHeader(refreshingTarget: self, refreshingAction: #selector(getOrderList))
        self.tableView.mj_footer = MJRefreshBackFooter(refreshingTarget: self, refreshingAction: #selector(getOrderListMore))
        self.tableView.tableFooterView = UIView()
        self.view.addSubview(self.tableView)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    @objc func getOrderList() {
        self.showHUB()
        self.page = 1
        let dict = ["date":self.timeTitleLabel.text!,"page":self.page] as [String : Any]
        DAPPWebService.shareNetWorkBase.RequestParams(url: "android/getUserBill", httpMethod: .HttpMethodPOST, params: ["jsonPara":DictToJson.convertData(dict)], success: { (response) in
            print(response)
            self.stopHUB()
            let status = response["code"] as! String
            if status == "0000"{
                self.dataArray = NSMutableArray()
                let data = response["data"] as! NSDictionary
                let bill = data["bill"] as! NSDictionary
                let list = bill["list"] as! NSArray
                self.dataArray.addObjects(from: list as! [Any])
                let profit = data["profit"] as! NSDictionary
                let monthMoney = profit["monthMoney"] as! NSNumber
                let dayMoney = profit["dayMoney"] as! NSNumber
                self.timeTipsLabel.text = self.timeTitleLabel.text! + "收款¥" + "\(monthMoney)" + " 今日收款¥" + "\(dayMoney)"
                self.tableView.reloadData()
            }else {
                self.view.makeToast(response["msg"] as? String)
            }
        }) { (error) in
            print(error)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as! TMKTradingListCell
        cell.selectionStyle = .none
        cell.setTradingListInfo(dataDict: self.dataArray[indexPath.row] as! NSDictionary)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    @objc func getOrderListMore() {
        self.showHUB()
        self.page += 1
        let dict = ["date":self.timeTitleLabel.text!,"page":self.page] as [String : Any]
        DAPPWebService.shareNetWorkBase.RequestParams(url: "android/getUserBill", httpMethod: .HttpMethodPOST, params: ["jsonPara":DictToJson.convertData(dict)], success: { (response) in
            print(response)
            self.stopHUB()
            let status = response["code"] as! String
            if status == "0000"{
                let data = response["data"] as! NSDictionary
                let bill = data["bill"] as! NSDictionary
                let list = bill["list"] as! NSArray
                self.dataArray.addObjects(from: list as! [Any])
                self.tableView.reloadData()
            }else {
                self.view.makeToast(response["msg"] as? String)
            }
        }) { (error) in
            print(error)
        }
    }
    @objc func chooseTime() {
        let dataPicker = EWYMDatePickerViewController()
        self.definesPresentationContext = true
        //回调显示方法
        dataPicker.backDate = { [weak self] date in
            self?.timeTitleLabel.text = date
            self?.getOrderList()
        }
        
        dataPicker.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        dataPicker.picker.reloadAllComponents()
        let times = getTimes()
        //弹出时日期滚动到当前日期效果
        self.present(dataPicker, animated: true) {
            dataPicker.picker.selectRow(times[0], inComponent: 0, animated: true)
            dataPicker.picker.selectRow(times[1] - 1, inComponent: 1, animated:   true)
        }
        self.tableView.mj_header.beginRefreshing()
    }
}
