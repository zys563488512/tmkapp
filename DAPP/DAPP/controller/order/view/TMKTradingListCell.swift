//
//  TMKTradingListCell.swift
//  DAPP
//
//  Created by yifutong on 2019/1/8.
//  Copyright © 2019 zys. All rights reserved.
//

import UIKit

class TMKTradingListCell: UITableViewCell {
    
//    var payImageView : UIImageView!
    var orderLabel : UILabel!
    var moneyLabel : UILabel!
    var statusLabel : UILabel!
    var timeLabel : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupUI() {
        self.orderLabel = UILabel()
        self.orderLabel.font = UIFont.boldSystemFont(ofSize: 16)
        self.orderLabel.textColor = UIColor.black
        self.contentView.addSubview(self.orderLabel)
        
        self.moneyLabel = UILabel()
        self.moneyLabel.font = UIFont.boldSystemFont(ofSize: 20)
        self.moneyLabel.textColor = kRGBColorFromHex(rgbValue: mainColor)
        self.moneyLabel.textAlignment = .right
        self.contentView.addSubview(self.moneyLabel)
        
        self.statusLabel = UILabel()
        self.statusLabel.textColor = UIColor.black
        self.statusLabel.font = UIFont.systemFont(ofSize: 14)
        self.contentView.addSubview(self.statusLabel)
        
        self.timeLabel = UILabel()
        self.timeLabel.textColor = UIColor.gray
        self.timeLabel.font = UIFont.systemFont(ofSize: 12)
        self.contentView.addSubview(self.timeLabel)
        
        self.orderLabel.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.width.equalTo(200)
            make.height.equalTo(20)
            make.top.equalTo(10)
        }
        self.statusLabel.snp.makeConstraints { (make) in
            make.height.equalTo(20)
            make.width.equalTo(100)
            make.top.equalTo(self.orderLabel.snp.bottom).offset(10)
            make.left.equalTo(15)
        }
        self.timeLabel.snp.makeConstraints { (make) in
            make.height.equalTo(20)
            make.width.equalTo(200)
            make.top.equalTo(self.statusLabel.snp.bottom).offset(10)
            make.left.equalTo(15)
        }
        self.moneyLabel.snp.makeConstraints { (make) in
            make.height.equalTo(20)
            make.centerY.equalTo(self.contentView)
            make.left.equalTo(screenWidth - 150)
            make.width.equalTo(135)
        }
    }
    func setTradingListInfo(dataDict:NSDictionary) {
       
        self.moneyLabel.text = dataDict["total_fee"] as? String
        let status = dataDict["status"] as! String
        self.timeLabel.text = dataDict["created_time"] as? String
        self.orderLabel.text = dataDict["trade_no"] as? String
        var statusStr:String!

        switch status {
        case "1":
            statusStr = "订单创建成功"
            break
        case "2":
            statusStr = "订单创建失败"
            break
        case "3":
            statusStr = "支付请求成功"
            break
        case "4":
            statusStr = "支付请求失败"
            break
        case "5":
            statusStr = "交易完成"
            break
        case "6":
            statusStr = "交易成功"
            break
        case "7":
            statusStr = "交易失败"
            break
        default:
            statusStr = "订单超时"
            break
        }
        self.statusLabel.text = statusStr
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
