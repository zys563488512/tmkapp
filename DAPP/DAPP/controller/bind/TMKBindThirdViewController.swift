//
//  TMKBindThirdViewController.swift
//  TMKProcess
//
//  Created by yifutong on 2018/12/26.
//  Copyright © 2018 yifutong. All rights reserved.
//

import UIKit

class TMKBindThirdViewController: TMKBaseViewController ,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    var auditRequestNo: String!
    
    var dataArray : NSArray!
    var tableView : UITableView!
    var bankUrl : String!
    var bankTextField:UITextField!
    var addressRightLabel: UILabel!
    var bankName : String!
    
    var userInfoDataDict : NSMutableDictionary!
    var dataDict : NSMutableDictionary!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "商户入网"
        self.userInfoDataDict = NSMutableDictionary()
        self.dataDict = NSMutableDictionary()
        print("----------------------------------")
        print(self.dataDict)
        print(self.userInfoDataDict)
        print("----------------------------------")
        self.view.addSubview(nvac)
        self.dataArray = [["image":"","name":"省市","isNext":"1"],
                          ["image":"","name":"详细地址","isNext":"0"]]
        
        self.tableView = UITableView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight - 50), style: .plain)
        self.tableView.backgroundColor = kRGBColorFromHex(rgbValue: bgMainColor)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.separatorInset = .zero
        self.tableView.tableFooterView = UIView()
        self.view.addSubview(self.tableView)
        
//        let footButton = UIButton(frame: CGRect(x: 0, y: screenHeight - 114, width: screenWidth, height: 50))
//        footButton.setTitleColor(UIColor.white, for: .normal)
//        footButton.backgroundColor = kRGBColorFromHex(rgbValue: mainColor)
//        footButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
//        footButton.addTarget(self, action: #selector(nextButton), for: .touchUpInside)
//        footButton.setTitle("完成", for: .normal)
//        self.view.addSubview(footButton)
        let btn1=UIButton(frame: CGRect(x: 10, y: 0, width: 40, height: 30))
        btn1.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn1.addTarget(self, action: #selector(nextButton), for: .touchUpInside)
        btn1.setTitle("完成", for: .normal)
        let item2=UIBarButtonItem(customView: btn1)
        self.navigationItem.rightBarButtonItem=item2
    }
    @objc func nextButton() {
//        self.getBankName()
//        self.dataDict.setValue(<#T##value: Any?##Any?#>, forKey: "<#T##String#>")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kShowHomePageNotification), object: nil)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let address = KLCityPickerView()
            address.areaPickerViewWithareaBlock { (province, city, area) in
//                self.currP = province
//                self.currC = city
//                self.currA = area
                self.addressRightLabel.text = province + city + area
//                    "\(String(describing: province!)) \(String(describing: city!)) \(String(describing: area!))" as String
            }

            break
        case 1:
            
            break
        default:
            break
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "cell")
        cell.selectionStyle = .none
        let dict = self.dataArray[indexPath.row] as! NSDictionary
        cell.textLabel?.text = dict["name"] as? String
        switch indexPath.row {
        case 0:
            self.addressRightLabel = UILabel(frame: CGRect(x: screenWidth - 200, y: 0, width: 170, height: 44))
            self.addressRightLabel.textAlignment = .right
            self.addressRightLabel.font = UIFont.systemFont(ofSize: 14)
            self.addressRightLabel.textColor = UIColor.black
            self.addressRightLabel.text = "请选择"
            cell.contentView.addSubview(self.addressRightLabel)
            break
        case 1:
            self.bankTextField = UITextField(frame: CGRect(x: 100, y: 0, width: screenWidth - 175, height: 44))
            self.bankTextField.placeholder = "请填写详细地址"
            self.bankTextField.tag = indexPath.row
            self.bankTextField.delegate = self
            self.bankTextField.font = UIFont.systemFont(ofSize: 14)
            self.bankTextField.textAlignment = .left
            cell.contentView.addSubview(self.bankTextField)
        default:
            break
        }
        if dict["isNext"] as! String == "1"{
            cell.accessoryType = .disclosureIndicator
        }
        return cell
    }
    @objc func uploadImage() {
        print("uploadImage")
        let sexActionSheet = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        
        weak var weakSelf = self
        
        let sexNanAction = UIAlertAction(title: "从相册中选择", style: .default){ (action:UIAlertAction)in
            
            weakSelf?.initPhotoPicker()
            //填写需要的响应方法
            
        }
        
        let sexNvAction = UIAlertAction(title: "拍照", style: .default){ (action:UIAlertAction)in
            
            
            weakSelf?.initCameraPicker()
            //填写需要的响应方法
            
        }
        
        
        let sexSaceAction = UIAlertAction(title: "取消", style: .cancel){ (action:UIAlertAction)in
            
            //填写需要的响应方法
            
        }
        
        
        sexActionSheet.addAction(sexNanAction)
        sexActionSheet.addAction(sexNvAction)
        sexActionSheet.addAction(sexSaceAction)
        
        self.present(sexActionSheet, animated: true, completion: nil)
        
    }
    @available(iOS 10.0, *)
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
//        if textField.tag == 0 {
//            self.mchName = textField.text
//        }else {
//            self.addressInfo = textField.text
//        }
    }
    //MARK: - 相机
    //从相册中选择
    func initPhotoPicker(){
        let photoPicker =  UIImagePickerController()
        photoPicker.delegate = self
        photoPicker.allowsEditing = true
        photoPicker.sourceType = .photoLibrary
        //在需要的地方present出来
        self.present(photoPicker, animated: true, completion: nil)
    }
    
    
    //拍照
    func initCameraPicker(){
        
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let  cameraPicker = UIImagePickerController()
            cameraPicker.delegate = self
            cameraPicker.allowsEditing = true
            cameraPicker.sourceType = .camera
            //在需要的地方present出来
            self.present(cameraPicker, animated: true, completion: nil)
        } else {
            self.view.makeToast("不支持拍照")
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //获得照片
        let image = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
        let data = image.jpegData(compressionQuality: 0.1)
        DAPPWebService.shareNetWorkBase.RequestImageParams(url: "mch/personal/fileReco", params: ["type":"2","file":data as Any], success: { (response) in
            print(response)
            let status = response["code"] as! String
            if status == "0000"{
                let data = response["data"] as! NSDictionary
                self.bankUrl = (data["url"] as! String)
                let certInfo = data["certInfo"] as! NSDictionary
                self.bankTextField.text = (certInfo["bank_card_num"] as! String)
            }else {
                self.view.makeToast(response["msg"] as? String)
            }
        }) { (error) in
            print(error)
        }
        // 拍照
        if picker.sourceType == .camera {
            //保存相册
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(image:didFinishSavingWithError:contextInfo:)), nil)
        }
        self.dismiss(animated: true, completion: nil)
    }
    func getBankName() {
        if self.bankTextField.text == "" || self.bankTextField.text == nil {
            self.view.makeToast("银行卡号不能为空")
            return
        }
        self.showHUB()
        DAPPWebService.shareNetWorkBase.RequestParamsAllUrl(url: "https://ccdcapi.alipay.com/validateAndCacheCardInfo.json", httpMethod: .HttpMethodGET, params: ["cardNo":self.bankTextField.text!,"cardBinCheck":"true"], success: { (response) in
            print(response)
            self.stopHUB()
            let validated = response["validated"] as! Bool
            if(validated) {
                self.bankName = (response["bank"] as! String)
                self.getNext()
            }else {
                self.view.makeToast("银行卡号有误，请确认")
            }
        }) { (error) in
            print(error)
        }
    }
    func getNext() {
        self.showHUB()
        DAPPWebService.shareNetWorkBase.RequestParams(url: "android/register", httpMethod: .HttpMethodPUT, params: ["step":"3","auditRequestNo":self.auditRequestNo,"openBank":self.bankName,"cardNo":self.bankTextField.text!], success: { (response) in
            print(response)
            self.stopHUB()
            let status = response["code"] as! String
            if status == "0000"{
                let data = response["data"] as! NSDictionary
                let qrCode = data["qrCode"] as! String
                save_userDefaultsStr(key: "qrCode", value: qrCode)
                save_userDefaults(key: "userInfo", value: response)
                self.navigationController?.popToRootViewController(animated: true)
            }else {
                self.view.makeToast(response["msg"] as? String)
            }
        }) { (error) in
            print(error)
        }
    }
    
    @objc func image(image:UIImage,didFinishSavingWithError error:NSError?,contextInfo:AnyObject) {
        
        if error != nil {
            print("保存失败")
        } else {
            print("保存成功")
        }
    }
}
