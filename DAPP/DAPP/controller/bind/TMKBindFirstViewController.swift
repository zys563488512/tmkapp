//
//  TMKBindFirstViewController.swift
//  TMKProcess
//
//  Created by yifutong on 2018/12/27.
//  Copyright © 2018 yifutong. All rights reserved.
//

import UIKit
class TMKBindFirstViewController: TMKBaseViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
  
    var idCardFImageView : UIImageView!
    var idCardFButton : UIButton!
    var idCardFRButton : UIButton!
    
    var idCardNImageView : UIImageView!
    var idCardNButton : UIButton!
    var idCardNRButton : UIButton!
    
    var idCardSImageView : UIImageView!
    var idCardSButton : UIButton!
    var idCardSRButton : UIButton!
    
    var userNameLabel : UILabel!
    var userNameText : UITextField!
    
    var userIdCardLabel : UILabel!
    var userIdCardText : UITextField!
    
    var currIdCardId : Int!
    
    var idCardF : String!
    var idCardN : String!
    var idCardS : String!
    
    var userInfoDataDict : NSMutableDictionary!
    var dataDict : NSMutableDictionary!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "身份证验证"
        self.view.addSubview(nvac)
        self.userInfoDataDict = NSMutableDictionary()
        self.dataDict = NSMutableDictionary()
        let btn1=UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 30))
        btn1.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn1.addTarget(self, action: #selector(nextButton), for: .touchUpInside)
        btn1.setTitle("下一步", for: .normal)
        let item2=UIBarButtonItem(customView: btn1)
        self.navigationItem.rightBarButtonItem=item2


        self.setupUI()
    }
    func setupUI() {
        self.idCardFImageView = UIImageView()
        self.idCardFImageView.layer.borderWidth = 1
        self.idCardFImageView.layer.borderColor = UIColor.gray.cgColor
        self.idCardFImageView.isUserInteractionEnabled = true
        self.idCardFImageView.backgroundColor = kRGBColorFromHex(rgbValue: 0xd1d1d1)
        self.idCardFImageView.layer.cornerRadius = 4
        self.view.addSubview(self.idCardFImageView)
        
        self.idCardFButton = UIButton()
        self.idCardFButton.setTitle("点击拍摄身份证正面", for: .normal)
        self.idCardFButton.tag = 1001
        self.idCardFButton.addTarget(self, action: #selector(self.uploadIdCardPhoto(btn:)), for: .touchUpInside)
        self.idCardFButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        self.idCardFButton.setTitleColor(UIColor.gray, for: .normal)
        self.idCardFImageView.addSubview(self.idCardFButton)
        
        self.idCardFRButton = UIButton()
        self.idCardFRButton.setTitle("重新拍摄", for: .normal)
        self.idCardFRButton.isHidden = true
        self.idCardFRButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        self.idCardFRButton.setTitleColor(UIColor.black, for: .normal)
        self.idCardFRButton.tag = 1001
        self.idCardFRButton.addTarget(self, action: #selector(self.uploadIdCardPhoto(btn:)), for: .touchUpInside)
        self.idCardFRButton.backgroundColor = UIColor.white
        self.idCardFRButton.layer.borderWidth = 1.0
        self.idCardFRButton.layer.cornerRadius = 4
        self.idCardFRButton.layer.borderColor = UIColor.black.cgColor
        self.idCardFImageView.addSubview(self.idCardFRButton)
        
        self.idCardFImageView.snp.makeConstraints { (make) in
            make.top.equalTo(20)
            make.height.equalTo(120)
            make.width.equalTo(240)
            make.centerX.equalTo(self.view)
        }
        self.idCardFButton.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.width.equalTo(240)
            make.top.equalTo(0)
            make.height.equalTo(120)
        }
        self.idCardFRButton.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.idCardFImageView)
            make.bottom.equalTo(-10)
            make.width.equalTo(80)
            make.height.equalTo(30)
        }
        
        self.idCardNImageView = UIImageView()
        self.idCardNImageView.isUserInteractionEnabled = true
        self.idCardNImageView.layer.borderWidth = 1
        self.idCardNImageView.layer.borderColor = UIColor.gray.cgColor
        self.idCardNImageView.backgroundColor = kRGBColorFromHex(rgbValue: 0xd1d1d1)
        self.idCardNImageView.layer.cornerRadius = 4
        self.view.addSubview(self.idCardNImageView)
        
        self.idCardNButton = UIButton()
        self.idCardNButton.setTitle("点击拍摄身份证反面", for: .normal)
        self.idCardNButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        self.idCardNButton.setTitleColor(UIColor.gray, for: .normal)
        self.idCardNButton.tag = 1002
        self.idCardNButton.addTarget(self, action: #selector(self.uploadIdCardPhoto(btn:)), for: .touchUpInside)
        self.idCardNImageView.addSubview(self.idCardNButton)
        
        self.idCardNRButton = UIButton()
        self.idCardNRButton.setTitle("重新拍摄", for: .normal)
        self.idCardNRButton.isHidden = true
        self.idCardNRButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        self.idCardNRButton.tag = 1002
        self.idCardNRButton.addTarget(self, action: #selector(self.uploadIdCardPhoto(btn:)), for: .touchUpInside)
        self.idCardNRButton.setTitleColor(UIColor.black, for: .normal)
        self.idCardNRButton.backgroundColor = UIColor.white
        self.idCardNRButton.layer.borderWidth = 1.0
        self.idCardNRButton.layer.cornerRadius = 4
        self.idCardNRButton.layer.borderColor = UIColor.black.cgColor
        self.idCardNImageView.addSubview(self.idCardNRButton)
        
        self.idCardNImageView.snp.makeConstraints { (make) in
            make.top.equalTo(self.idCardFImageView.snp.bottom).offset(10)
            make.height.equalTo(120)
            make.width.equalTo(240)
            make.centerX.equalTo(self.view)
        }
        self.idCardNButton.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.width.equalTo(240)
            make.height.equalTo(30)
            make.centerY.equalTo(self.idCardNImageView)
        }
        self.idCardNRButton.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.idCardNImageView)
            make.bottom.equalTo(-10)
            make.width.equalTo(80)
            make.height.equalTo(30)
        }
        
        self.idCardSImageView = UIImageView()
        self.idCardSImageView.isUserInteractionEnabled = true
        self.idCardSImageView.layer.borderWidth = 1
        self.idCardSImageView.layer.borderColor = UIColor.gray.cgColor
        self.idCardSImageView.backgroundColor = kRGBColorFromHex(rgbValue: 0xd1d1d1)
        self.idCardSImageView.layer.cornerRadius = 4
        self.view.addSubview(self.idCardSImageView)
        
        self.idCardSButton = UIButton()
        self.idCardSButton.setTitle("点击拍摄手持身份证照片", for: .normal)
        self.idCardSButton.tag = 1003
        self.idCardSButton.addTarget(self, action: #selector(self.uploadIdCardPhoto(btn:)), for: .touchUpInside)
        self.idCardSButton.tag = 1003
        self.idCardSButton.addTarget(self, action: #selector(self.uploadIdCardPhoto(btn:)), for: .touchUpInside)
        self.idCardSButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        self.idCardSButton.setTitleColor(UIColor.gray, for: .normal)
        self.idCardSImageView.addSubview(self.idCardSButton)
        
        self.idCardSRButton = UIButton()
        self.idCardSRButton.setTitle("重新拍摄", for: .normal)
        self.idCardSRButton.isHidden = true
        self.idCardSRButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        self.idCardSRButton.setTitleColor(UIColor.black, for: .normal)
        self.idCardSRButton.backgroundColor = UIColor.white
        self.idCardSRButton.layer.borderWidth = 1.0
        self.idCardSRButton.layer.cornerRadius = 4
        self.idCardSRButton.layer.borderColor = UIColor.black.cgColor
        self.idCardSImageView.addSubview(self.idCardSRButton)
        
        self.idCardSImageView.snp.makeConstraints { (make) in
            make.top.equalTo(self.idCardNImageView.snp.bottom).offset(10)
            make.height.equalTo(120)
            make.width.equalTo(240)
            make.centerX.equalTo(self.view)
        }
        self.idCardSButton.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.width.equalTo(240)
            make.centerY.equalTo(self.idCardSImageView)
        }
        self.idCardSRButton.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.idCardSImageView)
            make.bottom.equalTo(-10)
            make.width.equalTo(80)
            make.height.equalTo(30)
        }
        
        self.userNameLabel = UILabel()
        self.userNameLabel.font = UIFont.systemFont(ofSize: 14)
        self.userNameLabel.textColor = UIColor.black
        self.userNameLabel.text = "姓名"
        self.view.addSubview(self.userNameLabel)
        self.userNameText = UITextField()
        self.userNameText.placeholder = "请输入姓名"
        self.userNameText.font = UIFont.systemFont(ofSize: 14)
        self.view.addSubview(self.userNameText)
        
        let lineLabel = UILabel()
        lineLabel.backgroundColor = kRGBColorFromHex(rgbValue: 0xc1c1c1)
        self.view.addSubview(lineLabel)
        
        self.userNameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(self.idCardSImageView.snp.bottom).offset(15)
            make.width.equalTo(40)
            make.height.equalTo(30)
        }
        self.userNameText.snp.makeConstraints { (make) in
            make.left.equalTo(self.userNameLabel.snp.right).offset(10)
            make.top.equalTo(self.idCardSImageView.snp.bottom).offset(15)
            make.width.equalTo(screenWidth - 85)
            make.height.equalTo(30)
        }
        lineLabel.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(self.userNameText.snp.bottom)
            make.width.equalTo(screenWidth - 30)
            make.height.equalTo(1)
        }
        
        self.userIdCardLabel = UILabel()
        self.userIdCardLabel.font = UIFont.systemFont(ofSize: 14)
        self.userIdCardLabel.textColor = UIColor.black
        self.userIdCardLabel.text = "身份证号"
        self.view.addSubview(self.userIdCardLabel)
        self.userIdCardText = UITextField()
        self.userIdCardText.placeholder = "请输入身份证号码"
        self.userIdCardText.font = UIFont.systemFont(ofSize: 14)
        self.view.addSubview(self.userIdCardText)
        
        let lineLabel1 = UILabel()
        lineLabel1.backgroundColor = kRGBColorFromHex(rgbValue: 0xc1c1c1)
        self.view.addSubview(lineLabel1)
        
        self.userIdCardLabel.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(lineLabel.snp.bottom).offset(5)
            make.width.equalTo(60)
            make.height.equalTo(30)
        }
        self.userIdCardText.snp.makeConstraints { (make) in
            make.left.equalTo(self.userIdCardLabel.snp.right).offset(10)
            make.top.equalTo(lineLabel.snp.bottom).offset(5)
            make.width.equalTo(screenWidth - 85)
            make.height.equalTo(30)
        }
        lineLabel1.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(self.userIdCardText.snp.bottom)
            make.width.equalTo(screenWidth - 30)
            make.height.equalTo(1)
        }
    }
    @objc func nextButton() {
        if self.idCardF == "" || self.idCardF.count <= 0{
            self.view.makeToast("请上传身份证正面照")
            return
        }
        if self.idCardN == "" || self.idCardN.count <= 0{
            self.view.makeToast("请上传身份证反面照")
            return
        }
        if self.idCardS == "" || self.idCardS.count <= 0{
            self.view.makeToast("请上传手持身份证照片")
            return
        }
        if self.userNameText.text == "" || self.userNameText.text!.count <= 0{
            self.view.makeToast("请填写姓名")
            return
        }
        if self.userIdCardText.text == "" || self.userIdCardText.text!.count <= 0{
            self.view.makeToast("请身份证号码")
            return
        }
        
        self.dataDict.setValue(self.idCardF, forKey: "idcard_0")
        self.dataDict.setValue(self.idCardN, forKey: "idcard_1")
        self.dataDict.setValue(self.idCardS, forKey: "handcard")
        self.userInfoDataDict.setValue(self.userNameText.text!, forKey: "name")
        self.userInfoDataDict.setValue(self.userIdCardText.text!, forKey: "cert_no")
        let vc = TMKBindSecondViewController()
        vc.dataDict = self.dataDict
        vc.userInfoDataDict = self.userInfoDataDict
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK: 上传身份证相关照片
    @objc func uploadIdCardPhoto(btn:UIButton) {
        self.currIdCardId = btn.tag
        self.uploadImage()
    }
    //MARK: 重新上传身份证相关照片
    @objc func uploadImage() {
        print("uploadImage")
        let sexActionSheet = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        
        weak var weakSelf = self
        
        let sexNanAction = UIAlertAction(title: "从相册中选择", style: .default){ (action:UIAlertAction)in
            
            weakSelf?.initPhotoPicker()
            //填写需要的响应方法
            
        }
        
        let sexNvAction = UIAlertAction(title: "拍照", style: .default){ (action:UIAlertAction)in
            
            
            weakSelf?.initCameraPicker()
            //填写需要的响应方法
            
        }
        
        
        let sexSaceAction = UIAlertAction(title: "取消", style: .cancel){ (action:UIAlertAction)in
            
            //填写需要的响应方法
            
        }
        
        
        sexActionSheet.addAction(sexNanAction)
        sexActionSheet.addAction(sexNvAction)
        sexActionSheet.addAction(sexSaceAction)
        
        self.present(sexActionSheet, animated: true, completion: nil)

    }
    //MARK: - 相机
    //从相册中选择
    func initPhotoPicker(){
        let photoPicker =  UIImagePickerController()
        photoPicker.delegate = self
        photoPicker.allowsEditing = true
        photoPicker.sourceType = .photoLibrary
        //在需要的地方present出来
        self.present(photoPicker, animated: true, completion: nil)
    }
    
    
    //拍照
    func initCameraPicker(){
        
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let  cameraPicker = UIImagePickerController()
            cameraPicker.delegate = self
            cameraPicker.allowsEditing = true
            cameraPicker.sourceType = .camera
            //在需要的地方present出来
            self.present(cameraPicker, animated: true, completion: nil)
        } else {
            
            print("不支持拍照")
            
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //获得照片
        //        let image = info[.editedImage] as! UIImage
         let image = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
//        self.mchImageView.isEnabled = true
//        self.mchImageView.setImage(image, for: .normal)

//        let data = image.jpegData(compressionQuality: 0.1)
        
//        imageToBase64
        switch self.currIdCardId {
        case 1001:
            let dict = ["type":"1","base_64":imageToBase64(image: image)]
            let json = DictToJson.convertData(dict)
            DAPPWebService.shareNetWorkBase.RequestParams(url: "android/certificate", httpMethod: HttpMethod.HttpMethodPOST, params: ["jsonPara":json!], success: { (response) in
                print(response)
                let status = response["code"] as! String
                if status == "0000" {
                    
                }else if status == "0001" {
                    self.view.makeToast(response["msg"] as? String)
                }else {
                    self.view.makeToast(response["msg"] as? String)
                }
            }) { (error) in
                print(error)
            }
            self.idCardFButton.isHidden = true
            self.idCardFButton.isEnabled = false
            self.idCardFImageView.image = image
            self.idCardF = imageToBase64(image: image)
            self.idCardFRButton.isHidden = false
            break
        case 1002:
            self.idCardNButton.isHidden = true
            self.idCardNButton.isEnabled = false
            self.idCardN = imageToBase64(image: image)
            self.idCardNImageView.image = image
            self.idCardNRButton.isHidden = false
            break
        case 1003:
            self.idCardSButton.isHidden = true
            self.idCardSButton.isEnabled = false
            self.idCardS = imageToBase64(image: image)
            self.idCardSImageView.image = image
            self.idCardSRButton.isHidden = false
            break
        default:
            break
        }
        
        // 拍照
        if picker.sourceType == .camera {
            //保存相册
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(image:didFinishSavingWithError:contextInfo:)), nil)
        }
        
        //        personImage.image = image
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @objc func image(image:UIImage,didFinishSavingWithError error:NSError?,contextInfo:AnyObject) {
        
        if error != nil {
            
            print("保存失败")
            
            
        } else {
            
            print("保存成功")
            
            
        }
    }
}
