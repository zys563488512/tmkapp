//
//  TMKBindSecondViewController.swift
//  TMKProcess
//
//  Created by yifutong on 2018/12/26.
//  Copyright © 2018 yifutong. All rights reserved.
//

import UIKit
//import IQKeyboardManagerSwift

class TMKBindSecondViewController: TMKBaseViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,HQPickerViewDelegate{
    func pickerView(_ pickerView: UIPickerView!, didSelectText text: String!) {
        self.bankNameLabel.text = text
    }
    
    
    var bankFImageView : UIImageView!
    var bankFButton : UIButton!
    var bankFRButton : UIButton!
    
    var bankNImageView : UIImageView!
    var bankNButton : UIButton!
    var bankNRButton : UIButton!
    
    var userBankNumberLabel : UILabel!
    var userBankNumberText : UITextField!
    
    var bankNameTitleLabel : UILabel!
    var bankNameLabel : UILabel!
    var bankNameNextImageView : UIImageView!
    var bankNameChooseBtn : UIButton!
    
    var userIdCardLabel : UILabel!
    var userIdCardText : UITextField!
    
//    var pickerView: PickerView!
    var userInfoDataDict : NSMutableDictionary!
    var dataDict : NSMutableDictionary!
    
    var currBankId : Int!
    var bankF : String!
    var bankN : String!
    var bankArr : NSArray!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "借记卡信息"
        self.view.addSubview(nvac)
        self.userInfoDataDict = NSMutableDictionary()
        self.dataDict = NSMutableDictionary()
        self.bankArr = ["兴业银行",
                        "浦发银行",
                        "农业银行",
                        "光大银行",
                        "中国银行",
                        "工商银行",
                        "建设银行",
                        "北京银行",
                        "吉林银行",
                        "广州银行",
                        "广州农村商业银行",
                        "珠海华润银行",
                        "齐鲁银行",
                        "江苏银行",
                        "南京银行",
                        "哈尔滨银行",
                        "徽商银行",
                        "兰州银行",
                        "宁夏银行",
                        "渤海银行",
                        "河北银行",
                        "西安银行",
                        // "大连银行",
                        "郑州银行",
                        "宁波银行",
                        "中信银行",
                        "华夏银行",
                        "上海银行",
                        "招商银行",
                        "广发银行",
                        "平安银行",
                        "民生银行",
                        "北京农商",
                        "广州农商"]
        let btn1=UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 30))
        btn1.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn1.addTarget(self, action: #selector(nextButton), for: .touchUpInside)
        btn1.setTitle("下一步", for: .normal)
        let item2=UIBarButtonItem(customView: btn1)
        self.navigationItem.rightBarButtonItem=item2
        self.setupUI()
    }
    func setupUI() {
        self.bankFImageView = UIImageView()
        self.bankFImageView.layer.borderWidth = 1
        self.bankFImageView.layer.borderColor = UIColor.gray.cgColor
        self.bankFImageView.isUserInteractionEnabled = true
        self.bankFImageView.backgroundColor = kRGBColorFromHex(rgbValue: 0xd1d1d1)
        self.bankFImageView.layer.cornerRadius = 4
        self.view.addSubview(self.bankFImageView)
        
        self.bankFButton = UIButton()
        self.bankFButton.setTitle("点击拍摄银行卡正面", for: .normal)
        self.bankFButton.tag = 1001
        self.bankFButton.addTarget(self, action: #selector(self.uploadbankPhoto(btn:)), for: .touchUpInside)
        self.bankFButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        self.bankFButton.setTitleColor(UIColor.gray, for: .normal)
        self.bankFImageView.addSubview(self.bankFButton)
        
        self.bankFRButton = UIButton()
        self.bankFRButton.setTitle("重新拍摄", for: .normal)
        self.bankFRButton.isHidden = true
        self.bankFRButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        self.bankFRButton.setTitleColor(UIColor.black, for: .normal)
        self.bankFRButton.tag = 1001
        self.bankFRButton.addTarget(self, action: #selector(self.uploadbankPhoto(btn:)), for: .touchUpInside)
        self.bankFRButton.backgroundColor = UIColor.white
        self.bankFRButton.layer.borderWidth = 1.0
        self.bankFRButton.layer.cornerRadius = 4
        self.bankFRButton.layer.borderColor = UIColor.black.cgColor
        self.bankFImageView.addSubview(self.bankFRButton)
        
        self.bankFImageView.snp.makeConstraints { (make) in
            make.top.equalTo(20)
            make.height.equalTo(120)
            make.width.equalTo(240)
            make.centerX.equalTo(self.view)
        }
        self.bankFButton.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.width.equalTo(240)
            make.top.equalTo(0)
            make.height.equalTo(120)
        }
        self.bankFRButton.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.bankFImageView)
            make.bottom.equalTo(-10)
            make.width.equalTo(80)
            make.height.equalTo(30)
        }
        
        self.bankNImageView = UIImageView()
        self.bankNImageView.isUserInteractionEnabled = true
        self.bankNImageView.layer.borderWidth = 1
        self.bankNImageView.layer.borderColor = UIColor.gray.cgColor
        self.bankNImageView.backgroundColor = kRGBColorFromHex(rgbValue: 0xd1d1d1)
        self.bankNImageView.layer.cornerRadius = 4
        self.view.addSubview(self.bankNImageView)
        
        self.bankNButton = UIButton()
        self.bankNButton.setTitle("点击拍摄银行卡反面", for: .normal)
        self.bankNButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        self.bankNButton.setTitleColor(UIColor.gray, for: .normal)
        self.bankNButton.tag = 1002
        self.bankNButton.addTarget(self, action: #selector(self.uploadbankPhoto(btn:)), for: .touchUpInside)
        self.bankNImageView.addSubview(self.bankNButton)
        
        self.bankNRButton = UIButton()
        self.bankNRButton.setTitle("重新拍摄", for: .normal)
        self.bankNRButton.isHidden = true
        self.bankNRButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        self.bankNRButton.tag = 1002
        self.bankNRButton.addTarget(self, action: #selector(self.uploadbankPhoto(btn:)), for: .touchUpInside)
        self.bankNRButton.setTitleColor(UIColor.black, for: .normal)
        self.bankNRButton.backgroundColor = UIColor.white
        self.bankNRButton.layer.borderWidth = 1.0
        self.bankNRButton.layer.cornerRadius = 4
        self.bankNRButton.layer.borderColor = UIColor.black.cgColor
        self.bankNImageView.addSubview(self.bankNRButton)
        
        self.bankNImageView.snp.makeConstraints { (make) in
            make.top.equalTo(self.bankFImageView.snp.bottom).offset(10)
            make.height.equalTo(120)
            make.width.equalTo(240)
            make.centerX.equalTo(self.view)
        }
        self.bankNButton.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.width.equalTo(240)
            make.height.equalTo(30)
            make.centerY.equalTo(self.bankNImageView)
        }
        self.bankNRButton.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.bankNImageView)
            make.bottom.equalTo(-10)
            make.width.equalTo(80)
            make.height.equalTo(30)
        }
        
        self.userBankNumberLabel = UILabel()
        self.userBankNumberLabel.font = UIFont.systemFont(ofSize: 14)
        self.userBankNumberLabel.textColor = UIColor.black
        self.userBankNumberLabel.text = "银行卡号"
        self.view.addSubview(self.userBankNumberLabel)
        self.userBankNumberText = UITextField()
        self.userBankNumberText.placeholder = "请输入银行卡号"
        self.userBankNumberText.keyboardType = .numberPad
        self.userBankNumberText.font = UIFont.systemFont(ofSize: 14)
        self.view.addSubview(self.userBankNumberText)
        
        let lineLabel = UILabel()
        lineLabel.backgroundColor = kRGBColorFromHex(rgbValue: 0xc1c1c1)
        self.view.addSubview(lineLabel)
        
        self.userBankNumberLabel.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(self.bankNImageView.snp.bottom).offset(15)
            make.width.equalTo(60)
            make.height.equalTo(30)
        }
        self.userBankNumberText.snp.makeConstraints { (make) in
            make.left.equalTo(self.userBankNumberLabel.snp.right).offset(10)
            make.top.equalTo(self.bankNImageView.snp.bottom).offset(15)
            make.width.equalTo(screenWidth - 85)
            make.height.equalTo(30)
        }
        lineLabel.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(self.userBankNumberText.snp.bottom)
            make.width.equalTo(screenWidth - 30)
            make.height.equalTo(1)
        }
        
        self.bankNameTitleLabel = UILabel()
        self.bankNameTitleLabel.textColor = UIColor.black
        self.bankNameTitleLabel.text = "开户银行"
        self.bankNameTitleLabel.font = UIFont.systemFont(ofSize: 14)
        self.view.addSubview(self.bankNameTitleLabel)
        self.bankNameLabel = UILabel()
        self.bankNameLabel.font = UIFont.systemFont(ofSize: 14)
        self.bankNameLabel.text = "工商银行"
        self.bankNameLabel.textAlignment = .right
        self.view.addSubview(self.bankNameLabel)
        self.bankNameNextImageView = UIImageView()
        self.bankNameNextImageView.image = UIImage(named: "main_arrow")
        self.view.addSubview(self.bankNameNextImageView)
        let lineLabel2 = UILabel()
        lineLabel2.backgroundColor = kRGBColorFromHex(rgbValue: 0xc1c1c1)
        self.view.addSubview(lineLabel2)
        self.bankNameChooseBtn = UIButton()
        self.bankNameChooseBtn.addTarget(self, action: #selector(self.chooseBankName), for: .touchUpInside)
        self.view.addSubview(self.bankNameChooseBtn)
        
        self.bankNameTitleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(lineLabel.snp.bottom).offset(5)
            make.width.equalTo(60)
            make.height.equalTo(30)
        }
        self.bankNameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.bankNameTitleLabel.snp.right).offset(5)
            make.top.equalTo(lineLabel.snp.bottom).offset(5)
            make.width.equalTo(screenWidth - 125)
            make.height.equalTo(30)
        }
        self.bankNameNextImageView.snp.makeConstraints { (make) in
            make.left.equalTo(self.bankNameLabel.snp.right).offset(10)
            make.top.equalTo(lineLabel.snp.bottom).offset(14)
            make.width.equalTo(8)
            make.height.equalTo(12)
//            make.centerX.equalTo(self.bankNameLabel)
        }
        self.bankNameChooseBtn.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.top.equalTo(lineLabel.snp.bottom)
            make.width.equalTo(screenWidth)
            make.height.equalTo(30)
        }
        lineLabel2.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(self.bankNameLabel.snp.bottom)
            make.width.equalTo(screenWidth-30)
            make.height.equalTo(1)
        }
        
        self.userIdCardLabel = UILabel()
        self.userIdCardLabel.font = UIFont.systemFont(ofSize: 14)
        self.userIdCardLabel.textColor = UIColor.black
        self.userIdCardLabel.text = "银行预留手机号"
        self.view.addSubview(self.userIdCardLabel)
        self.userIdCardText = UITextField()
        self.userIdCardText.keyboardType = .numberPad
        self.userIdCardText.placeholder = "请输入银行预留手机号"
        self.userIdCardText.font = UIFont.systemFont(ofSize: 14)
        self.view.addSubview(self.userIdCardText)
        
        let lineLabel1 = UILabel()
        lineLabel1.backgroundColor = kRGBColorFromHex(rgbValue: 0xc1c1c1)
        self.view.addSubview(lineLabel1)
        
        self.userIdCardLabel.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(lineLabel2.snp.bottom).offset(5)
            make.width.equalTo(100)
            make.height.equalTo(30)
        }
        self.userIdCardText.snp.makeConstraints { (make) in
            make.left.equalTo(self.userIdCardLabel.snp.right).offset(10)
            make.top.equalTo(lineLabel2.snp.bottom).offset(5)
            make.width.equalTo(screenWidth - 85)
            make.height.equalTo(30)
        }
        lineLabel1.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(self.userIdCardText.snp.bottom)
            make.width.equalTo(screenWidth - 30)
            make.height.equalTo(1)
        }
    }
    //MARK: 单项选择
    @objc func chooseBankName() {
        self.userIdCardText.resignFirstResponder()
        self.userBankNumberText.resignFirstResponder()
        let picker = HQPickerView(frame: self.view.bounds)
        picker.delegate = self
        picker.customArr = self.bankArr as? [Any]
        self.view.addSubview(picker)
    }
   
    @objc func nextButton() {
        self.dataDict.setValue(self.bankF, forKey: "bankcard_0")
        self.dataDict.setValue(self.bankN, forKey: "bankcard_1")
        self.userInfoDataDict.setValue(self.userBankNumberText.text!, forKey: "account_no")
        self.userInfoDataDict.setValue(self.userIdCardText.text!, forKey: "mobile")
        self.userInfoDataDict.setValue(self.bankNameLabel.text!, forKey: "account_bank")
        let vc = TMKBindThirdViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK: 借记卡信息认证
    @objc func uploadbankPhoto(btn:UIButton) {
        self.currBankId = btn.tag
        self.uploadImage()
    }
    @objc func uploadImage() {
        let sexActionSheet = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        weak var weakSelf = self
//        let sexNanAction = UIAlertAction(title: "从相册中选择", style: .default){ (action:UIAlertAction)in
//            weakSelf?.initPhotoPicker()
//            //填写需要的响应方法
//        }
        let sexNvAction = UIAlertAction(title: "拍照", style: .default){ (action:UIAlertAction)in
             weakSelf?.initCameraPicker()
            //填写需要的响应方法
        }
        let sexSaceAction = UIAlertAction(title: "取消", style: .cancel){ (action:UIAlertAction)in
            //填写需要的响应方法
        }
//        sexActionSheet.addAction(sexNanAction)
        sexActionSheet.addAction(sexNvAction)
        sexActionSheet.addAction(sexSaceAction)
        self.present(sexActionSheet, animated: true, completion: nil)
    }
    //MARK: - 相机
    //从相册中选择
    func initPhotoPicker(){
        let photoPicker =  UIImagePickerController()
        photoPicker.delegate = self
        photoPicker.allowsEditing = true
        photoPicker.sourceType = .photoLibrary
        //在需要的地方present出来
        self.present(photoPicker, animated: true, completion: nil)
    }
    //拍照
    func initCameraPicker(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let  cameraPicker = UIImagePickerController()
            cameraPicker.delegate = self
            cameraPicker.allowsEditing = true
            cameraPicker.sourceType = .camera
            //在需要的地方present出来
            self.present(cameraPicker, animated: true, completion: nil)
        } else {
            self.view.makeToast("相机不可用")
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //获得照片
        let image = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
        
        switch self.currBankId {
        case 1001:
            let dict = ["type":"2","base_64":imageToBase64(image: image)]
            let json = DictToJson.convertData(dict)
            DAPPWebService.shareNetWorkBase.RequestParams(url: "android/certificate", httpMethod: HttpMethod.HttpMethodPOST, params: ["jsonPara":json!], success: { (response) in
                print(response)
                let status = response["code"] as! String
                if status == "0000"{
                    
                }else {
                    self.view.makeToast(response["msg"] as? String)
                }
            }) { (error) in
                print(error)
            }
            self.bankFButton.isHidden = true
            self.bankFButton.isEnabled = false
            self.bankFImageView.image = image
            self.bankF = imageToBase64(image: image)
            self.bankFRButton.isHidden = false
            break
        case 1002:
            self.bankNButton.isHidden = true
            self.bankNButton.isEnabled = false
            self.bankN = imageToBase64(image: image)
            self.bankNImageView.image = image
            self.bankNRButton.isHidden = false
            break
        default:
            break
        }
        
        // 拍照
        if picker.sourceType == .camera {
            //保存相册
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(image:didFinishSavingWithError:contextInfo:)), nil)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @objc func image(image:UIImage,didFinishSavingWithError error:NSError?,contextInfo:AnyObject) {
        
        if error != nil {
            print("保存失败")
        } else {
            print("保存成功")
        }
    }
}
