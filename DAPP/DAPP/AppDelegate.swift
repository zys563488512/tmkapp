//
//  AppDelegate.swift
//  DAPP
//
//  Created by yifutong on 2018/12/26.
//  Copyright © 2018 yifutong. All rights reserved.
//

import UIKit
import CoreData
import AVFoundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,XGPushDelegate,AVSpeechSynthesizerDelegate {
    var window: UIWindow?
    var myPlayer : AVAudioPlayer!
    var filePath: String!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//        let deviceToken = get_userDefaultsStr(key: "deviceToken")
//        if deviceToken == nil || deviceToken == "" {
            XGPush.defaultManager().isEnableDebug = true
//            let action1 = XGNotificationAction.action(withIdentifier: "xgaction001", title: "xgAction1", options: .none)
//            let action2 = XGNotificationAction.action(withIdentifier: "xgaction002", title: "xgAction2", options: .destructive)
//        if (action1 != nil) && (action2 != nil) {
//            let category = XGNotificationCategory.category(withIdentifier: "xgCategory", actions: [action1 as! XGNotificationAction,action2 as! XGNotificationAction], intentIdentifiers: [], options: [])
//            let configure = XGNotificationConfigure(notificationWithCategories: category as? Set<XGNotificationCategory>, types: XGUserNotificationTypes(rawValue: XGUserNotificationTypes.alert.rawValue|XGUserNotificationTypes.badge.rawValue|XGUserNotificationTypes.sound.rawValue))
//            if (configure != nil) {
//                XGPush.defaultManager().notificationConfigure = configure
//            }
//        }
            XGPush.defaultManager().startXG(withAppID: 2200323016, appKey: "I47888XNXYTH", delegate: self)
//            XGPush.defaultManager().reportXGNotificationInfo(launchOptions!)
//        }
        if get_userDefaultsStr(key: "token") == nil || (get_userDefaultsStr(key: "token"))! == "" {
            self.showLoginViewController()
        }else {
            self.showHomeViewController()
        }
//        self.conbindAudio()
        self.window?.makeKeyAndVisible()
        NotificationCenter.default.addObserver(self, selector: #selector(showLoginViewController), name: NSNotification.Name(rawValue: kShowLoginPageNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showHomeViewController), name: NSNotification.Name(rawValue: kShowHomePageNotification), object: nil)
        return true
    }
    @objc func showLoginViewController () {
        let nav  = TMKBaseNavigationController(rootViewController: TMKLoginViewController())
        self.window?.rootViewController = nav
    }
    @objc func showHomeViewController () {
        let navc = TMKBaseTabBarController()
        self.window?.rootViewController = navc
    }
    @available(iOS 10.0, *)
    private func xgPush(_ center: UNUserNotificationCenter, willPresent notification: UNNotification?, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        XGPush.defaultManager().reportXGNotificationInfo((notification?.request.content.userInfo)!)
        
        completionHandler(UNNotificationPresentationOptions(rawValue: 0 |  UNNotificationPresentationOptions.badge.rawValue | UNNotificationPresentationOptions.sound.rawValue | UNNotificationPresentationOptions.alert.rawValue))
    }
  
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any],          fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void){
        print("userInfo-----------\(userInfo)")
        XGPush.defaultManager().reportXGNotificationInfo(userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
//    // iOS 10 新增回调 API
//    // App 用户点击通知
//    // App 用户选择通知中的行为
//    // App 用户在通知中心清除消息
//    // 无论本地推送还是远程推送都会走这个回调
//    #if __IPHONE_OS_VERSION_MAX_ALLOWED >=     __IPHONE_10_0
    @available(iOS 10.0, *)
    func xgPush(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse?, withCompletionHandler completionHandler: @escaping () -> Void) {
        XGPush.defaultManager().reportXGNotificationResponse(response)
        completionHandler()
        
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "DAPP")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if #available(iOS 10.0, *) {
            _ = persistentContainer.viewContext
        } else {
            // Fallback on earlier versions
        }
    }

    /**
     收到推送的回调
     @param application  UIApplication 实例
     @param userInfo 推送时指定的参数
     @param completionHandler 完成回调
     */
    func xgPushDidReceiveRemoteNotification(_ notification: Any, withCompletionHandler completionHandler: ((UInt) -> Void)? = nil) {
        if notification is NSDictionary {
            let dict = notification as! NSDictionary
            let cus = dict["custom"] as! String
            let custom = dictionaryWithJsonString(str: cus)
            if custom["code"] as! NSNumber == 103 {
                self.window?.makeToast(custom["msg"] as? String)
                self.showLoginViewController()
            }else {
                self.conbindAudio(str: custom["msg"] as! String)
            }
//            print("notification ------ \(dict["body"] as! String)")
//            let str = dict["body"] as! String
            
        }else {
            if #available(iOS 10.0, *) {
                let content = (notification as AnyObject).request.content
                print("content ------ \(content.body)")
                let str = content.body
                self.conbindAudio(str: str)
            } else {
                // Fallback on earlier versions
            }
            
        }
    }
    func xgPushDidSetBadge(_ isSuccess: Bool, error: Error?) {

    }
    func xgPushDidFinishStop(_ isSuccess: Bool, error: Error?) {

    }
    func xgPushDidFinishStart(_ isSuccess: Bool, error: Error?) {
        print("\(isSuccess)")
    }
    func xgPushDidReportNotification(_ isSuccess: Bool, error: Error?) {

    }
    func xgPushDidRegisteredDeviceToken(_ deviceToken: String?, error: Error?) {
        save_userDefaultsStr(key: "deviceToken", value: deviceToken!)
        print("----deviceToken ------ \(String(describing: deviceToken))")
    }
    private func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        print("userInfo==\(userInfo)")
    }
    func application(_ application: UIApplication, didFailToContinueUserActivityWithType userActivityType: String, error: Error) {
        print("userActivityType ---- \(userActivityType)")
    }
    func conbindAudio(str:String){
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            if #available(iOS 10.0, *) {
                try audioSession.setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
            } else {
                // Fallback on earlier versions
            }
        }catch let error as NSError{
            print(error.code)
        }
        if get_userDefaultsStr(key: "isPlay") != nil && (get_userDefaultsStr(key: "isPlay"))! != ""{
            SpeechUtteranceManager.shared.speechWeather(with: NSLocalizedString(str, comment: ""))
        }
    }
}

