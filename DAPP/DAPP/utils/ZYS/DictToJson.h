//
//  DictToJson.h
//  DAPP
//
//  Created by yifutong on 2019/1/23.
//  Copyright © 2019 zys. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DictToJson : NSObject
+ (NSString *)convertToJsonData:(NSDictionary *)dict;
@end

