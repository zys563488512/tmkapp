//
//  EWDatePickerPresentAnimated.swift
//  DAPP
//
//  Created by yifutong on 2019/2/20.
//  Copyright © 2019 zys. All rights reserved.
//

import UIKit
enum EWYMDatePickerPresentAnimateType {
    case present//被推出时
    case dismiss//取消时
}
class EWYMDatePickerPresentAnimated: NSObject,UIViewControllerAnimatedTransitioning {
    var type: EWYMDatePickerPresentAnimateType = .present
    
    init(type: EWYMDatePickerPresentAnimateType) {
        self.type = type
    }
    /// 动画时间
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    /// 动画效果
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        switch type {
        case .present:
            let toVC : EWYMDatePickerViewController = transitionContext.viewController(forKey: .to) as! EWYMDatePickerViewController
            let toView = toVC.view
            
            let containerView = transitionContext.containerView
            containerView.addSubview(toView!)
            
            toVC.containV.transform = CGAffineTransform(translationX: 0, y: (toVC.containV.frame.height))
            
            UIView.animate(withDuration: 0.25, animations: {
                /// 背景变色
                toVC.backgroundView.alpha = 1.0
                /// datepicker向上推出
                toVC.containV.transform =  CGAffineTransform(translationX: 0, y: -10)
            }) { (finished) in
                UIView.animate(withDuration: 0.2, animations: {
                    /// transform初始化
                    toVC.containV.transform = CGAffineTransform.identity
                }, completion: { (finished) in
                    transitionContext.completeTransition(true)
                })
            }
        case .dismiss:
            let toVC : EWYMDatePickerViewController = transitionContext.viewController(forKey: .from) as! EWYMDatePickerViewController
            
            UIView.animate(withDuration: 0.25, animations: {
                toVC.backgroundView.alpha = 0.0
                /// datepicker向下推回
                toVC.containV.transform =  CGAffineTransform(translationX: 0, y: (toVC.containV.frame.height))
            }) { (finished) in
                transitionContext.completeTransition(true)
            }
        }
    }
}
