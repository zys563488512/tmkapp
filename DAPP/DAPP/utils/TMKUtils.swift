//
//  TMKUtils.swift
//  TMKProcess
//
//  Created by yifutong on 2018/12/26.
//  Copyright © 2018 yifutong. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
//import CommonCrypto

let BaseUrl = "https://cards.yifubank.com/"
let BD_APP_ID = "15387600"
let BD_API_KEY = "dP2fmDgjY6E1wyWeZoexbgpM"
let BD_SECRET_KEY = "MZQFCzLPlKjDliKGT3773gF59yUiATx9"
let mainColor = 0x1E90FF
let bgMainColor = 0xF1F1F1
let navColor = 0x1E90FF


//RGBA
func kRGBColorFromHex(rgbValue: Int) -> (UIColor) {
    
    return UIColor(red: ((CGFloat)((rgbValue & 0xFF0000) >> 16)) / 255.0,
                   green: ((CGFloat)((rgbValue & 0xFF00) >> 8)) / 255.0,
                   blue: ((CGFloat)(rgbValue & 0xFF)) / 255.0,
                   alpha: 1.0)
}

//应用程序信息
let appDisplayName = Bundle.main.infoDictionary?["CFBundleDisplayName"] //程序名称
let majorVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"]//主程序版本号
let minorVersion = Bundle.main.infoDictionary?["CFBundleVersion"] //版本号（内部标示）
let appVersion = majorVersion as! String
// 当前系统版本
let currentVersion = (UIDevice.current.systemVersion as NSString).floatValue

// 屏幕宽度
let screenHeight = UIScreen.main.bounds.height
// 屏幕高度
let screenWidth = UIScreen.main.bounds.width
// 默认图片
let defaultImg = UIImage(named: "photo_define")
// NSUserDefault
let userDefault = UserDefaults()
// 通知中心
let notice = NotificationCenter.default
//屏幕分辨率比例
let screenScale:CGFloat = UIScreen.main.responds(to: #selector(getter: UIScreen.main.scale)) ? UIScreen.main.scale : 1.0
//不同屏幕尺寸字体适配（375，667是因为目前苹果开发一般用IPHONE6做中间层 如果不是则根据实际情况修改）
//相对于iPhone6的宽度比例
let screenWidthRatio:CGFloat =  screenHeight / 375
let screenHeightRatio:CGFloat = screenWidth / 667

//根据传入的值算出乘以比例之后的值
func adaptedWidth(width:CGFloat) ->CGFloat {
    return CGFloat(ceil(Float(width))) * screenWidthRatio
}

func adaptedHeight(height:CGFloat) ->CGFloat {
    return CGFloat(ceil(Float(height))) * screenHeightRatio
}
//判断是那种设备

/*
 4  4s
 */
func iPhone4() ->Bool {
    return UIScreen.main.bounds.size.height == 480.0
}

/*
 5  5s
 */
func iPhone5() ->Bool {
    return UIScreen.main.bounds.size.height == 568.0
}

/*
 6  6s  7
 */
func iPhone6() ->Bool {
    return UIScreen.main.bounds.size.height == 667.0
}

/*
 6plus  6splus  7plus
 */
func iPhone6plus() ->Bool {
    return UIScreen.main.bounds.size.height == 736.0
}

func iPhoneX() -> Bool {
    return UIScreen.main.bounds.size.height == 812.0
}
let kScreenBounds = UIScreen.main.bounds


/* app版本  以及设备系统版本 */
let infoDictionary            = Bundle.main.infoDictionary
let kAppName: String?         = infoDictionary!["CFBundleDisplayName"] as? String        /* App名称 */
let kAppVersion: String?      = infoDictionary!["CFBundleShortVersionString"] as? String /* App版本号 */
let kAppBuildVersion: String? = infoDictionary!["CFBundleVersion"] as? String            /* Appbuild版本号 */
let kAppBundleId: String?     = infoDictionary!["CFBundleIdentifier"] as? String                 /* app bundleId */
let platformName: String?     = infoDictionary!["DTPlatformName"] as? String  //平台名称（iphonesimulator 、 iphone）


/* 检查系统版本 */

//版本号相同
func systemVersionEqual(version:String) ->Bool {
    return UIDevice.current.systemVersion == version
}

//系统版本高于等于该version  测试发现只能传入带一位小数点的版本号  不然会报错    具体原因待探究
func systemVersionGreaterThan(version:String) ->Bool{
    return UIDevice.current.systemVersion.compare(version, options: .numeric, range: version.startIndex..<version.endIndex, locale: Locale(identifier:version)) != ComparisonResult.orderedAscending
}


//系统版本低于等于该version  测试发现只能传入带一位小数点的版本号  不然会报错    具体原因待探究
func systemVersionLessThan(version:String) ->Bool{
    return UIDevice.current.systemVersion.compare(version, options: .numeric, range: version.startIndex..<version.endIndex, locale: Locale(identifier:version)) != ComparisonResult.orderedDescending
}
//圆
extension UIImage {
    //生成圆形图片
    func toCircle() -> UIImage {
        //取最短边长
        let shotest = min(self.size.width, self.size.height)
        //输出尺寸
        let outputRect = CGRect(x: 0, y: 0, width: shotest, height: shotest)
        //开始图片处理上下文（由于输出的图不会进行缩放，所以缩放因子等于屏幕的scale即可）
        UIGraphicsBeginImageContextWithOptions(outputRect.size, false, 0)
        let context = UIGraphicsGetCurrentContext()!
        //添加圆形裁剪区域
        context.addEllipse(in: outputRect)
        context.clip()
        //绘制图片
        self.draw(in: CGRect(x: (shotest-self.size.width)/2,
                             y: (shotest-self.size.height)/2,
                             width: self.size.width,
                             height: self.size.height))
        //获得处理后的图片
        let maskedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return maskedImage
    }
}
func randomRGB() -> UIColor {
     return UIColor.init(red: CGFloat(arc4random()%256)/255.0, green: CGFloat(arc4random()%256)/255.0, blue: CGFloat(arc4random()%256)/255.0, alpha: 1)
}
///////////////////////////////////////////////////////////////////////////////////////////////////
//
//  3. catch缓存文件夹和Documents文件夹
//
///////////////////////////////////////////////////////////////////////////////////////////////////

let XHUserDefault = UserDefaults.standard


/// Cache缓存文件夹
let cacheDir = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last

/// Documents文件夹
let documentsDir = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first

////判断是不是plus
//let currentModeSize = UIScreen.main.currentMode?.size
//let isPlus = UIScreen.instancesRespond(to: #selector(getter: RunLoop.currentMode)) ? CGSize(width: 1242, height: 2208).equalTo(currentModeSize) : false
////判断字符串是否为空
//func trimString(str:String)->String{
//    var nowStr = str.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
//    return nowStr
//}
//
////去除空格和回车
//func trimLineString(str:String)->String{
//    var nowStr = str.str
//
//
//
//
//
//
//
//
//
//
//
//        str.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
//    return nowStr
//}
////根据键盘监控  获取键盘高度
//func getKeyBoardHeight(aNotification:NSNotification)->CGFloat {
//    var uInfo = aNotification.userInfo as NSDictionary
//    let avalue = uInfo?["UIKeyboardFrameEndUserInfoKey"] as NSValue
//    let keyrect : CGRect = avalue.CGRectValue()
//    let keyheight : CGFloat = keyrect.size.height
//    return keyheight
//}
////获取目录下存储的json文件并解析为集合
//func getNativeJson(filename : String,fileext : String)->AnyObject{
//    let pathsBun = NSBundle.mainBundle()
//    let paths = pathsBun.pathForResource(filename, ofType : fileext)
//    var errors:NSError?
//    var content : NSData = NSData(contentsOfFile: paths!, options : .DataReadingMappedIfSafe, error: nil)!
//    var returneddata: AnyObject?  = JSONSerialization.JSONObjectWithData(content as NSData, options:NSJSONReadingOptions.MutableContainers, error:&errors)
//    return returneddata!
//}
////消息提醒
//func showAlertView(#title:String,#message:String)
//{
//    var alert = UIAlertView()
//    alert.title = title
//    alert.message = message
//    alert.addButtonWithTitle("好")
//    alert.show()
//}
//获取本地存储数据
func get_userDefaults(key : String)->AnyObject?{
    
    var saveStr : AnyObject! = userDefault.object(forKey: key) as AnyObject
    saveStr = (saveStr == nil) ? "" as AnyObject : saveStr
    return saveStr
}
func get_userDefaultsStr(key : String)->String?{
    
    var saveStr : String! = userDefault.object(forKey: key) as? String
    saveStr = (saveStr == nil) ? "" as String : saveStr
    return saveStr
}
//存储数据
func save_userDefaults(key : String,value:AnyObject?){
    userDefault.set(value!, forKey: key)
}
func save_userDefaultsStr(key : String,value:String?){
    userDefault.set(value!, forKey: key)
}
//删除
func remove_userDefaults(key:String){
    userDefault.removeObject(forKey: key)
}

////字符串转数组
//func stringToArray(str:String)->NSArray{
//    var dataArray:[String] = []
//    for items in str{
//        dataArray.append("\(items)")
//    }
//    return dataArray
//}
func getTimes() -> [Int] {
    
    var timers: [Int] = [] //  返回的数组
    
    let calendar: Calendar = Calendar(identifier: .gregorian)
    var comps: DateComponents = DateComponents()
    comps = calendar.dateComponents([.year,.month,.day, .weekday, .hour, .minute,.second], from: Date())
    
    timers.append(comps.year! % 2000)  // 年 ，后2位数
    timers.append(comps.month!)            // 月
    timers.append(comps.day!)                // 日
    timers.append(comps.hour!)               // 小时
    timers.append(comps.minute!)            // 分钟
    timers.append(comps.second!)            // 秒
    timers.append(comps.weekday! - 1)      //星期
    
    return timers;
}

//md5 16
extension String {
    var md5 : String{
        let cString = self.cString(using: String.Encoding.utf8)
        let length = CUnsignedInt(
            self.lengthOfBytes(using: String.Encoding.utf8)
        )
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(
            capacity: Int(16)
        )
        
        CC_MD5(cString!, length, result)
        
        return String(format:
            "%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                      result[0], result[1], result[2], result[3],
                      result[4], result[5], result[6], result[7],
                      result[8], result[9], result[10], result[11],
                      result[12], result[13], result[14], result[15])
    }
    //#pragma mark - 16位 小写
    
    var md5HashToLower16Bit: String{
        var str:NSString!
        let md5Str:NSString = self.md5 as NSString
        for _ in 0..<24 {
            str = md5Str.substring(with: NSRange(location: 8, length: 16)) as NSString
        }
        return str as String
    }
//    #pragma mark - 32位 小写
//
//    - (NSString *)md5HashToLower32Bit {
//    const char *input = [self UTF8String];
//    unsigned char result[CC_MD5_DIGEST_LENGTH];
//    CC_MD5(input, (CC_LONG)strlen(input), result);
//
//    NSMutableString *digest = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
//    for (NSInteger i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
//    [digest appendFormat:@"%02x", result[i]];
//    }
//
//    return digest;
//    }
//
//    #pragma mark - 32位 大写
//
//    - (NSString *)md5HashToUpper32Bit {
//    const char *input = [self UTF8String];
//    unsigned char result[CC_MD5_DIGEST_LENGTH];
//    CC_MD5(input, (CC_LONG)strlen(input), result);
//
//    NSMutableString *digest = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
//    for (NSInteger i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
//    [digest appendFormat:@"%02X", result[i]];
//    }
//
//    return digest;
//    }
    
    
    
//    #pragma mark - 16位 大写
//
//    - (NSString *)md5HashToUpper16Bit {
//    NSString *md5Str = [self md5HashToUpper32Bit];
//
//    NSString *string;
//    for (int i=0; i<24; i++) {
//    string=[md5Str substringWithRange:NSMakeRange(8, 16)];
//    }
//
//    return string;
//    }
    
}

class SpeechUtteranceManager: NSObject {
    
    /// 单例管理语音播报 比较适用于多种类型语音播报管理
    public static let shared = SpeechUtteranceManager()
    
    var synthesizer = AVSpeechSynthesizer()
    var speechUtterance: AVSpeechUtterance?
    var voiceType = AVSpeechSynthesisVoice(language: "zh-TW")
    
    private override init() {
        super.init()
        do {
            if #available(iOS 10.0, *) {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: AVAudioSession.CategoryOptions.duckOthers)
            } else {
                // Fallback on earlier versions
            }
        } catch {
            print(error.localizedDescription)
        }
        synthesizer.delegate = self
    }
    
    /// 自定义语音播报方法
    /// 此处只举例播报一个String的情况
    func speechWeather(with weather: String) {
        if let _ = speechUtterance {
            synthesizer.stopSpeaking(at: .immediate)
        }
        
        do {
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print(error.localizedDescription)
        }
        
        speechUtterance = AVSpeechUtterance(string: weather)
        speechUtterance?.voice = voiceType
        speechUtterance?.rate = 0.5
        synthesizer.speak(speechUtterance!)
    }
}

extension SpeechUtteranceManager: AVSpeechSynthesizerDelegate {
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        do {
            try AVAudioSession.sharedInstance().setActive(false, options: .notifyOthersOnDeactivation)
        } catch {
            print(error.localizedDescription)
        }
        speechUtterance = nil
    }
}

func dictionaryWithJsonString(str : String) -> NSDictionary {
    if str == "" {
        return [:]
    }
    let jsonData = str.data(using: .utf8)
    let d = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableContainers)
    let dic = d as! NSDictionary
    return dic
    
}
func imageToBase64(image:UIImage) -> String {
    let imgData:NSData = image.jpegData(compressionQuality: 1.0)! as NSData
    return imgData.base64EncodedString(options: .lineLength64Characters)
}

extension Date {
    
    //格式化日期
    func getDateString() -> String{
        let  dateFormater = DateFormatter.init()
        //EEEE:星期几
        //YYYY:年份
        //MM:月份
        //dd:几号
        //HH:小时
        //mm:分钟
        //ss:秒
        //zzz:时区号
        
        dateFormater.dateFormat = "EEEE-YYYY-MM-dd HH:mm:ss:zzz"
        let dateStr = dateFormater.string(from: self)
        
        return dateStr
    }
    //格式化日期
    func getDateYMD() -> String{
        let  dateFormater = DateFormatter.init()
        //YYYY:年份
        //MM:月份
        //dd:几号
        //HH:小时
        //mm:分钟
        //ss:秒
        //zzz:时区号
        
        dateFormater.dateFormat = "YYYY-MM-dd HH:mm:ss:zzz"
        let dateStr = dateFormater.string(from: self)
        
        return dateStr
    }
    //格式化日期
    func getDateYM() -> String{
        let  dateFormater = DateFormatter.init()
        //YYYY:年份
        //MM:月份
        //dd:几号
        //HH:小时
        //mm:分钟
        //ss:秒
        //zzz:时区号
        
        dateFormater.dateFormat = "YYYY-MM"
        let dateStr = dateFormater.string(from: self)
        
        return dateStr
    }
    //获取某个日期为星期几
    func getDateWeekday() ->String{
        let weekdaysTitleArr = ["周日","周一","周二","周三","周四","周五","周六"]
        let timeInterval:TimeInterval = self.timeIntervalSince1970
        let days = Int(timeInterval/86400)
        let weekday = ((days + 4)%7+7)%7
        return weekdaysTitleArr[weekday]
    }
    
    //将日期转换为时间戳(10位)
    func getTimeStamp() -> Int{
        let timeInterval:TimeInterval = self.timeIntervalSince1970
        let timeStamp = Int(timeInterval)
        return timeStamp
    }
}
class Utility: NSObject {
    
    //根据时间戳获取时间字符串
    class func getDateStringFromTimestamp(_ timestamp:Int64) ->String{
        let timeInterval:TimeInterval = TimeInterval(timestamp)
        let date = Date(timeIntervalSince1970: timeInterval)
        return date.getDateString()
    }
    
    //根据时间戳获取日期对象
    class func getDateFromTimestamp(_ timestamp:Int64) ->Date{
        let timeInterval:TimeInterval = TimeInterval(timestamp)
        let date = Date(timeIntervalSince1970: timeInterval)
        return date
    }
    
    //获取两个日期的间隔天数
    class func getDateInterval(dateA:Date,dateB:Date) ->Int{
        let interval = dateA.timeIntervalSince(dateB)
        let days = Int(interval/(24*60*60))
        //        let hours = Int(interval/(60*60))
        //        let mins = Int(interval/60)
        return days
    }
}
