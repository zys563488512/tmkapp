//
//  DAPPWebService.swift
//  DAPP
//
//  Created by zys on 2018/12/6.
//  Copyright © 2018年 zys. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

enum HttpMethod {
    
    case HttpMethodGET
    case HttpMethodPOST
    case HttpMethodPUT
    case HttpMethodDELETE
}

final class DAPPWebService: NSObject {
    var baseUrl:String = BaseUrl
    var isNeedAccessToken:Bool = false //是否拼接AccessToken
    var baseParams:NSMutableDictionary = [:] //公共参数
    
    static let shareNetWorkBase = DAPPWebService()
    
    private override init() {
        
    }
    
    //MARK:拼接完整的url地址
    func getCompleteUrl(url:String) -> String {
        let newUrl = self.baseUrl.appending(url)
        if self.isNeedAccessToken {
            //拼接AccessToken
        }
        return newUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
    }
    //MARK:拼接完整的参数
    func CombinationParams(params:NSDictionary) -> NSDictionary {
        self.baseParams.addEntries(from: params as! [AnyHashable: Any]) //拼接字符串
        return self.baseParams
    }
    //MARK:获取请求方式
    func getHttpMethod(method:HttpMethod) -> HTTPMethod {
        if method == .HttpMethodPOST {
            return .post
        }else if method == .HttpMethodGET {
            return .get
        }else if method == .HttpMethodPUT{
            return .put
        }else {
            return .delete
        }
    }
    //MARK:上传数据：post or get
    func RequestParamsAllUrl(url:String,httpMethod:HttpMethod,params:NSMutableDictionary,success:@escaping (AnyObject)->(),failure:@escaping (Error)->()) -> Void {
        if !self.monitorNet() {
            print("网络错误")
            return
        }
        let allParams = self.CombinationParams(params: params)
        var token = get_userDefaultsStr(key: "token")
        if token == nil {
            token = ""
        }else {
            
        }
        let headers: HTTPHeaders = [:]
        print(self.getCompleteUrl(url: url))
        print(allParams)
        let method = self.getHttpMethod(method: httpMethod)
        
        
        Alamofire.request(url, method: method, parameters: allParams as? Parameters, encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(let value):
                let dic:NSDictionary = value as! NSDictionary
                success(dic)
                
                break
            case .failure(let error):
                failure(error)
                break
            }
        }
    }
    
    
    
    
    //MARK:上传数据：post or get
    func RequestParams(url:String,httpMethod:HttpMethod,params:NSMutableDictionary,success:@escaping (AnyObject)->(),failure:@escaping (Error)->()) -> Void {
        if !self.monitorNet() {
            print("网络错误")
            return
        }
        params.setValue("1.0", forKey: "version")
        
        var token = get_userDefaultsStr(key: "token")
        if token == nil {
            token = ""
        }else {
            params.setValue(token, forKey: "token")
        }
        let allParams = self.CombinationParams(params: params)
        let headers: HTTPHeaders = [:]
        print(self.getCompleteUrl(url: url))
        print(allParams)
        let method = self.getHttpMethod(method: httpMethod)

        
        Alamofire.request(self.getCompleteUrl(url: url), method: method, parameters: allParams as? Parameters, encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in

            switch(response.result) {
                case .success(let value):
                    let dic:NSDictionary = value as! NSDictionary
                    let status = dic["code"] as! String
                    if status == "1000" {
                        remove_userDefaults(key: "token")
                        remove_userDefaults(key: "qrCode")
                        remove_userDefaults(key: "userInfo")
                        remove_userDefaults(key: "deviceToken")
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kShowLoginPageNotification), object: nil)
                    }
                    success(dic)
                    
                break
                case .failure(let error):
                    failure(error)
                break
            }
        }
    }
    
    
    //MARK:上传带图片的数据
    //注意，图片必须为Data||NSData类型，其他参数尽量传String或者NSString
    func RequestImageParams(url:String,params:NSMutableDictionary,success:@escaping (AnyObject)->(),failure:@escaping (Error)->()) -> Void {
        if !self.monitorNet() {
            print("网络错误")
            return
        }
        var token = get_userDefaultsStr(key: "token")
        if token == nil {
            token = ""
        }else {
            
        }
        let headers: HTTPHeaders = [
            "version" :"1.0.0",
            "token" : token!
        ]
        print(self.getCompleteUrl(url: url))
        Alamofire.upload(multipartFormData:{ multipartFormData in
            for (key, value) in params {
                
                if value is Data || value is NSData {
                    
                    let imageName = String(describing: NSDate()).appending(".png")
                    multipartFormData.append(value as! Data, withName: key as! String, fileName: imageName, mimeType: "image/png")
                } else {
                    let str:String = value as! String
                    multipartFormData.append(str.data(using: .utf8)!, withName: key as! String)
                }
            }},
         to:self.getCompleteUrl(url: url),
         method:.post,
         headers:headers,
         encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (response:DataResponse<Any>) in
                    switch(response.result) {
                    case .success(let value):
                        let dic:NSDictionary = value as! NSDictionary
                        success(dic)
                        break
                    case .failure(let error):
                        failure(error)
                        break
                    }
                })
                break
            case .failure(let error):
                failure(error)
                break
            }
        })
    }
    
    //MARK:https证书验证
    func verificationCertificate() -> Void {
        
        let manager = SessionManager.default
        
        manager.delegate.sessionDidReceiveChallenge = { session, challenge in
            
            var disposition: URLSession.AuthChallengeDisposition = .performDefaultHandling
            var credential: URLCredential?
            
            if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
                
                disposition = URLSession.AuthChallengeDisposition.useCredential
                credential = URLCredential.init(trust: challenge.protectionSpace.serverTrust!)
                
            } else {
                if challenge.previousFailureCount > 0 {
                    
                    disposition = .cancelAuthenticationChallenge
                    
                } else {
                    
                    credential = manager.session.configuration.urlCredentialStorage?.defaultCredential(for: challenge.protectionSpace)
                    
                    if credential != nil {
                        disposition = .useCredential
                    }
                }
            }
            
            return (disposition, credential)
        }
    }
    

    func monitorNet() -> Bool {
        let manager = NetworkReachabilityManager(host: "www.baidu.com")
        manager?.listener = { status in
            print("网络状态:\(status)")
        }
        let isSuccess = manager?.startListening() //开始监听网络
        if isSuccess! {
            manager?.stopListening()
        }
        return isSuccess!
    }
}
