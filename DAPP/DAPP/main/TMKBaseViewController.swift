//
//  TMKBaseViewController.swift
//  TMKProcess
//
//  Created by yifutong on 2018/12/26.
//  Copyright © 2018 yifutong. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import NVActivityIndicatorView
import Kingfisher
import SnapKit
class TMKBaseViewController: UIViewController {
    var nvac :NVActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        self.view.backgroundColor = kRGBColorFromHex(rgbValue: bgMainColor)
        self.navigationController?.navigationBar.isHidden = false
        nvac = NVActivityIndicatorView(frame: CGRect(origin: CGPoint(x: screenWidth/2 - 50, y: screenHeight/2 - 50), size: CGSize(width: 100, height: 100)), type: .ballClipRotatePulse, color: kRGBColorFromHex(rgbValue: mainColor), padding: 1)
        self.view.bringSubviewToFront(nvac)
    }
    func showHUB() {
        nvac.startAnimating()
    }
    func stopHUB() {
        nvac.stopAnimating()
    }

}
