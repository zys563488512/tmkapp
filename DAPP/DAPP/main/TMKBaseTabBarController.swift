//
//  TMKBaseTabBarController.swift
//  TMKProcess
//
//  Created by yifutong on 2018/12/26.
//  Copyright © 2018 yifutong. All rights reserved.
//

import UIKit

class TMKBaseTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let tabbar = UITabBar.appearance()
        
        tabbar.tintColor = kRGBColorFromHex(rgbValue: mainColor)
        tabbar.barTintColor = UIColor.white
        /// 添加子控制器
        addChildViewControllers()
    }
    
    // 设置控制器
    private func setChildController(controller: UIViewController, imageName: String) {
        
        controller.tabBarItem.image = UIImage(named: imageName + "_nor")
        controller.tabBarItem.selectedImage = UIImage(named: imageName + "_pressed")
    }
  
    // 添加子控制器
    private func addChildViewControllers() {
        setChildViewController(TMKHomeViewController(), title: "提现", imageName: "ic_menu_home")
        setChildViewController(TMKOrderListViewController(), title: "账单", imageName: "ic_menu_order")
        setChildViewController(TMKMineViewController(), title: "个人中心", imageName: "ic_menu_me")
    }
    
    // 初始化子控制器
    private func setChildViewController(_ childController: UIViewController, title: String, imageName: String) {
        // 设置 tabbar 文字和图片
        setChildController(controller: childController, imageName: imageName)
        childController.title = title
        // 添加导航控制器为 TabBarController 的子控制器
        addChild(TMKBaseNavigationController(rootViewController: childController))
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
